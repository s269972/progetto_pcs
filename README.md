# CutPolygon - Computational Science

Quest'applicazione, dato un poligono qualsiasi, ed un segmento, consente di "tagliare" tale poligono con il dato segmento,
tramite l'utilizzo della retta che passa per quest'ultimo.

## Esempio

PRIMA DEL TAGLIO

![example](Images/t3.png)

DOPO IL TAGLIO

![example_result](Images/t3t.png)

## Requirements

![plantuml](Images/FINALE_UML.png)

Scrivere un codice sfruttando il design pattern "Dependency Injection" della programmazione a oggetti 
che permetta di "prendere" in input un poligono e restituire in output uno o più poligoni, a seconda del tipo di taglio, e del tipo di poligono iniziale. 

(Nota: dovrà essere utilizzata una forma semplificata della Dependency Injection, ovvero senza l'utilizzo dell' injector)

In particolare il programma dovrà basarsi su tre funzionalità / step.

### 1 Step: Crezione del Poligono

- Implementare il metodo "CreatePolygon" della classe GeometryFactory in modo tale che dato in input un elenco di punti nello spazio 2D, sotto forma di vettore di "Point", genera un poligono di tipo "Polygon" (guardare l'uml), i cui "polygonPoints" siano un vettore di "Point" contenente l'elenco dato in input, e i cui "Vertici" siano un vettore di "int" che contenga i vertici del poligono creato. Questi vertici devono partire da 0. I punti dell'elenco in input sono dati in senso ANTIORARIO. Il metodo deve restituire un intero, valore che è associato all' ID del poligono. Questo numero parte da 1. 

- Il metodo "GetPolygon", dato in input l'ID del poligono, restituisce il poligono associato.
Se è inserito un valore associato ad un poligono non creato, il metodo deve restituire l'eccezione: "Polygon not found".

- Lo stesso vale per il metodo "GetPolygonNumberVertices", che però restituisce il numero di Vertici del poligono. La stessa eccezione è lanciata nel caso di inserimento di ID non esistente.

- Il metodo "GetPolygonVertex" prende in input l'ID del poligono e la posizione del vertice da cercare e restituisce il vertice del poligono associato a quell'ID alla posizione passata. Se non esiste nessun poligono  associato all'ID passato, l'eccezione "Polygon not found" è lanciata. Se non esiste nessun vertice di quel poligono alla posizione passata, è lanciata l'eccezione "Vertex not found".

- TUTTE LE OPERAZIONI MATEMATICHE DOVRANNO TENERE CONTO DI UNA TOLLERANZA COMPUTAZIONALE DEL VALORE DI: 1.0E-7

### 2 Step: Calcolo delle intersezioni

- Implementare il metodo "ComputeIntersection" della classe Intersector1D1D in modo tale che dati in input un poligono creato con la GeometryFactory e il suo corrispettivo segmento tagliante (il segmento è sotto forma di un vettore di due Vector2d della libreria Eigen o NumPy) calcola l'intersezione tra il segmento ed ogni lato del poligono. Ogni lato del poligono è il vettore che congiunge due vertici successivi del poligono. Il metodo restituisce il numero di intersezioni tra il segmento e il poligono. Ogni informazione sulle intersezioni (tipo di intersezione, coordinate parametriche dell'intersezione, etc..) devono essere salvati come attributi della classe Intersector1D1D utilizzando le strutture dati adatte (vettori, mappe, etc..). 

- I metodi "SetFirstSegment" e "SetSecondSegment" sono due metodi che non devono ritornare nulla, ma sono fondamentali per la corretta riuscita del calcolo delle intersezioni. Il primo metodo deve impostare il primo tra i due segmenti di cui si vuole calcolare l'intersezione, che sarà sempre il segmento tagliante, passato in input. Il secondo deve naturalemente impostare il secondo segmento di cui si vuole calcolare l'intersezione, che sarà ognuno dei lati del poligono. E' importante fare attenzione che per il calcolo dell'intersezione si abbiano entrambi i segmenti interessati sotto forma di Vector2d delle librerie Eigen o NumPy per poter riuscire a svolgere i calcoli matematici più facilmente, e per poter sfruttare le funzioni delle librerie.

- Il metodo "GetPolygonNumberVertices" prende in input il poligono di cui si deve calcolare l'intersezione e restituisce il numero di vertici di questo poligono. Non lancia eccezioni.

- Il metodo "IntersectionPoint" restituisce il punto di intersezione tra segmento tagliante e poligono, alla posizione passata input. Non lancia eccezioni.

- TUTTE LE OPERAZIONI MATEMATICHE DOVRANNO TENERE CONTO DI UNA TOLLERANZA COMPUTAZIONALE DEL VALORE DI: 1.0E-7

### 3 Step: Taglio del poligono

- Il metodo "CutPolygon" della classe WorkPolygon è il metodo centrale del programma. In questo metodo avviene l'effettivo "taglio" del poligono. Il metodo prende in input il segmento tagliante e il poligono da tagliare. "CutPolygon" deve restituire l'ID dell'operazione di taglio, associata al/i poligono/i ottenuto/i dal taglio. Questo metodo deve salvarsi le coordinate dei vertici del/i nuovo/i poligono/i e l'elenco dei vertici. Ai nuovi punti e ad i nuovi vertici dovranno essere aggiunte anche le coordinate e il vertice corrispondente dei punti di intersezione tra il segmento tagliante e il poligono, e le coordinate e il vertice corrispondente dei due estremi del segmento tagliante qual'ora fossero interni al poligono.
    Nei casi in cui l'intersezione sia del tipo "IntersectionParallelOnLine" o se il segmento interseca il poligono su delle "punte" cioè se le posizioni (ovvero le coordinate parametriche) dei lati del poligono intersecati siano o "Begin" o "End" allora "CutPolygon" non genererà nessun nuovo punto. Nella struttura dati che conterrà le coordinate dei nuovi punti dovranno essere inseriti prima le coordinate dei punti del poligono passati in input, poi le coordinate dei punti estremi del segmento tagliante qual'ora i requirements ne prevedano l'aggiunta (vedi subito sopra) ed infine le coordinate dei punti di intersezione qual'ora i requirements ne prevedano l'aggiunta (vedi subito sopra).

- Il metodo "GetCuttedPolygons" prendendo in input l'ID dell'operazione di taglio restituisce un vettore di "Polygon" contenente i poligoni tagliati dell'operazione corrispondente all'ID. Nel caso sia passato un ID non valito, il programma deve restituire l'eccezione "CuttedPolygon not found".

- Il metodo "GetNewPoints" prendendo in input sempre l'ID dell'operazione di taglio restituisce un vettore di "Point" contenente il vettore delle coordinate dei nuovi punti generati dal taglio, con l'ordine subito sopra indicato. Nel caso sia passato un ID non valido, il programma deve restituire l'eccezione "Polygon not cutted".

- Il metodo "Cointained" verifica la posizione nello spazio bidimensionale di un punto rispetto ad un segmento (destra, sinistra o contenuto).

- Con la funzione "CreateVectorNewPoint" devono essere salvati in un vettore di "Point" i vertici del poligono da tagliare, le intersezioni e gli eventuali punti estremi del segmento interni al poligono. Questo vettore sarà utilizzato dal metodo "CutPolygon" sopra citato.
    Deve inoltre essere creato il vettore _newPoint che salva gli stessi punti (ATTENZIONE ai doppioni!).

- Il metodo "ClimbSegment" deve aggiungere al tempPolygon tutti i vertici della retta passante per il  fino all'intersezione successiva, percorrendola nel verso delle coordinate parametriche decrescenti.

- Il metodo "DescentSegment" deve aggiungere al tempPolygon tutti i vertici della retta passante per il  fino all'intersezione successiva, percorrendola nel verso delle coordinate parametriche crescenti.

- TUTTE LE OPERAZIONI MATEMATICHE DOVRANNO TENERE CONTO DI UNA TOLLERANZA COMPUTAZIONALE DEL VALORE DI: 1.0E-7

## Test eseguiti

### Test 1
Prima del taglio:
![example](Images/t1.png)
Dopo il taglio:
![example](Images/t1t.png)
### Test 2
Prima del taglio:
![example](Images/t2.png)
Dopo il taglio:
![example](Images/t2t.png)
### Test 3
Prima del taglio:
![example](Images/t3.png)
Dopo il taglio:
![example](Images/t3t.png)
### Test Professor D'Auria 1
Prima del taglio:
![example](Images/tDA1.png)
Dopo il taglio:
![example](Images/tDA1t.png)
### Test Professor D'Auria 2
Prima del taglio:
![example](Images/tDA2.png)
Dopo il taglio:
![example](Images/tDA2t.png)
### Test poligono Convesso
Prima del taglio:
![example](Images/tc.png)
Dopo il taglio:
![example](Images/tct.png)
