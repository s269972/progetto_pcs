#ifndef __TEST_CUTPOLYGON_H
#define __TEST_CUTPOLYGON_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Eigen"

#include "ComputeIntersection.hpp"
#include "CutPolygon.hpp"
#include "createPolygon.hpp"
#include "DTO.hpp"

using namespace ComputationalScienceLibrary;
using namespace testing;
using namespace std;

namespace ComputationalScienceTesting {

void FillPolygon1Vertices(vector<Point>& vertices)
{
  vertices.push_back(Point(1.0000e+00, 1.0000e+00));
  vertices.push_back(Point(5.0000e-00, 1.0000e+00));
  vertices.push_back(Point(5.0000e-00, 3.1000e+00));
  vertices.push_back(Point(1.0000e+00, 3.1000e+00));
}

void FillSegment1Vertices(vector<Point>& segment)
{
    segment.push_back(Point(2.0000e+00, 1.2000e+00));
    segment.push_back(Point(4.0000e+00, 3.0000e+00));
}

  TEST(TestComputationalScience, TestCreatePolygon1)
  {
    vector<Point> vertices1;
    FillPolygon1Vertices(vertices1);

    GeometryFactory geometryFactory;

    try
    {
      EXPECT_EQ(geometryFactory.CreatePolygon(vertices1), 1);

      const int idPolygon = geometryFactory.CreatePolygon(vertices1);
      const Polygon polygon = geometryFactory.GetPolygon(idPolygon);
      EXPECT_EQ(polygon.polygonPoints[1].X, vertices1[1].X);
      EXPECT_EQ(polygon.polygonPoints[1].Y, vertices1[1].Y);
      EXPECT_EQ(polygon.Vertici[0], 0);


      EXPECT_EQ(geometryFactory.GetPolygonNumberVertices(idPolygon), 4);

      const Point& vertex = geometryFactory.GetPolygonVertex(idPolygon, 2);
      EXPECT_EQ(vertex.X, vertices1[2].X);
      EXPECT_EQ(vertex.Y, vertices1[2].Y);

    }
    catch (const exception& exception)
    {
      FAIL();
    }

    try
    {
      geometryFactory.GetPolygon(12);
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Polygon not found"));
    }

    try
    {
      geometryFactory.GetPolygonVertex(12, 2);
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Polygon not found"));
    }

    try
    {
      geometryFactory.GetPolygonVertex(1, 17);
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Vertex not found"));
    }

  }

  TEST(TestComputationalScience, TestComputeIntersection1)
  {
      vector<Point> vertices1;
      FillPolygon1Vertices(vertices1);
      vector<Point> segment1;
      FillSegment1Vertices(segment1);

      GeometryFactory geometryFactory;
      const int idPolygon = geometryFactory.CreatePolygon(vertices1);
      const Polygon polygon = geometryFactory.GetPolygon(idPolygon);

      try
      {
          Intersector1D1D intersector1D1D;
          const int numIntersectionPoints = intersector1D1D.ComputeIntersection(polygon, segment1);
          EXPECT_EQ(numIntersectionPoints, 2);
          EXPECT_EQ(intersector1D1D.GetPolygonNumberVertices(polygon), 4);
          EXPECT_EQ(intersector1D1D.IntersectionPoint(0).X, 1.777777777777778);
          EXPECT_EQ(intersector1D1D.IntersectionPoint(0).Y, 1.0000e+00);
          EXPECT_EQ(intersector1D1D.IntersectionPoint(1).X, 4.111111111111111);
          EXPECT_EQ(intersector1D1D.IntersectionPoint(1).Y, 3.1000e+00);
      }

      catch (const exception& exception)
      {
        FAIL();
      }

  }

  TEST(TestComputationalScience, TestCutPolygon1)
  {
      vector<Point> vertices1;
      FillPolygon1Vertices(vertices1);
      vector<Point> segment1;
      FillSegment1Vertices(segment1);

      //create polygon and segment
      GeometryFactory geometryFactory;
      const int idPolygon = geometryFactory.CreatePolygon(vertices1);
      const Polygon polygon_to_cut = geometryFactory.GetPolygon(idPolygon);

      //compute the intersection between the polygon and the segment, and save the results
      Intersector1D1D intersector1D1D;
      const int numIntersectionPoints = intersector1D1D.ComputeIntersection(polygon_to_cut, segment1);
      vector<Point> intersectionPoints = intersector1D1D._intersectionPoints;
      vector<double> parametricCoordinates = intersector1D1D._everyResultParametricCoordinates;

      //Create parameters to method ClimbSegment and DescentiSegment
      Vector2d vectorSegment (segment1[1].X - segment1[0].X, segment1[1].Y - segment1[0].Y );

      Polygon tempPolygonClimb;
      tempPolygonClimb.Vertici.push_back(0);
      tempPolygonClimb.Vertici.push_back(4);
      tempPolygonClimb.polygonPoints.push_back(vertices1[0]);
      tempPolygonClimb.polygonPoints.push_back(intersectionPoints[0]);

      Polygon tempPolygonDescent;
      tempPolygonDescent.Vertici.push_back(1);
      tempPolygonDescent.Vertici.push_back(2);
      tempPolygonDescent.Vertici.push_back(7);
      tempPolygonDescent.polygonPoints.push_back(vertices1[1]);
      tempPolygonDescent.polygonPoints.push_back(vertices1[2]);
      tempPolygonClimb.polygonPoints.push_back(intersectionPoints[1]);

      try
      {
        WorkPolygon workPolygon;
        const int operationId = workPolygon.CutPolygon(polygon_to_cut,
                                                       segment1);
        EXPECT_EQ(operationId, 1);
        vector<Polygon> cuttedPolygons = workPolygon.GetCuttedPolygons(operationId);
        EXPECT_EQ(cuttedPolygons[1].Vertici[1], 2);
        EXPECT_EQ(cuttedPolygons[0].Vertici[3], 6);
        vector<Point> newPoints = workPolygon.GetNewPoints(operationId);
        EXPECT_EQ(newPoints[3].X, vertices1[3].X);
        EXPECT_EQ(newPoints[3].Y, vertices1[3].Y);
        EXPECT_EQ(newPoints[5].X, segment1[0].X);
        EXPECT_EQ(newPoints[5].Y, segment1[0].Y);
        EXPECT_EQ(newPoints[7].X, intersectionPoints[1].X);
        EXPECT_EQ(newPoints[7].Y, intersectionPoints[1].Y);
        vector<Point> tempNewPoint = workPolygon.CreateVectorNewPoint(intersectionPoints, parametricCoordinates, polygon_to_cut, segment1, operationId);
        EXPECT_EQ(tempNewPoint[3].X, vertices1[3].X);
        EXPECT_EQ(tempNewPoint[3].Y, vertices1[3].Y);
        EXPECT_EQ(tempNewPoint[5].X, segment1[0].X);
        EXPECT_EQ(tempNewPoint[5].Y, segment1[0].Y);
        int startIntersectionClimb = workPolygon.ClimbSegment(operationId, 4, tempNewPoint, polygon_to_cut, vectorSegment, tempPolygonClimb);
        EXPECT_EQ(startIntersectionClimb, 2);
        int startIntersectionDescent = workPolygon.DescentSegment(operationId, 7, tempNewPoint, polygon_to_cut, vectorSegment, tempPolygonDescent);
        EXPECT_EQ(startIntersectionDescent, 0);
      }

      catch (const exception& exception)
      {
        FAIL();
      }

      try
      {
        WorkPolygon workPolygon;
        workPolygon.GetCuttedPolygons(2);
        FAIL();
      }
      catch (const exception& exception)
      {
        EXPECT_THAT(std::string(exception.what()), Eq("CuttedPolygon not found"));
      }

      try
      {
        WorkPolygon workPolygon;
        workPolygon.GetNewPoints(2);
        FAIL();
      }
      catch (const exception& exception)
      {
        EXPECT_THAT(std::string(exception.what()), Eq("Polygon not cutted"));
      }

  }

void FillPolygon2Vertices(vector<Point>& vertices)
{
  vertices.push_back(Point(2.5000e+00, 1.0000e+00));
  vertices.push_back(Point(4.0000e-00, 2.1000e+00));
  vertices.push_back(Point(3.4000e-00, 4.2000e+00));
  vertices.push_back(Point(1.6000e+00, 4.2000e+00));
  vertices.push_back(Point(1.0000e+00, 2.1000e+00));
}

void FillSegment2Vertices(vector<Point>& segment)
{
    segment.push_back(Point(1.4000e+00, 2.7500e+00));
    segment.push_back(Point(3.6000e+00, 2.2000e+00));
}

TEST(TestComputationalScience, TestCreatePolygon2)
{
  vector<Point> vertices2;
  FillPolygon2Vertices(vertices2);

  GeometryFactory geometryFactory;

  try
  {
    EXPECT_EQ(geometryFactory.CreatePolygon(vertices2), 1);

    const int idPolygon = geometryFactory.CreatePolygon(vertices2);
    const Polygon polygon = geometryFactory.GetPolygon(idPolygon);
    EXPECT_EQ(polygon.polygonPoints[1].X, vertices2[1].X);
    EXPECT_EQ(polygon.polygonPoints[1].Y, vertices2[1].Y);
    EXPECT_EQ(polygon.Vertici[0], 0);


    EXPECT_EQ(geometryFactory.GetPolygonNumberVertices(1), 5);

    const Point& vertex = geometryFactory.GetPolygonVertex(1, 2);
    EXPECT_EQ(vertex.X, vertices2[2].X);
    EXPECT_EQ(vertex.Y, vertices2[2].Y);

  }
  catch (const exception& exception)
  {
    FAIL();
  }

  try
  {
    geometryFactory.GetPolygon(12);
    FAIL();
  }
  catch (const exception& exception)
  {
    EXPECT_THAT(std::string(exception.what()), Eq("Polygon not found"));
  }

  try
  {
    geometryFactory.GetPolygonVertex(12, 2);
    FAIL();
  }
  catch (const exception& exception)
  {
    EXPECT_THAT(std::string(exception.what()), Eq("Polygon not found"));
  }

  try
  {
    geometryFactory.GetPolygonVertex(1, 17);
    FAIL();
  }
  catch (const exception& exception)
  {
    EXPECT_THAT(std::string(exception.what()), Eq("Vertex not found"));
  }
}

TEST(TestComputationalScience, TestComputeIntersection2)
{
    vector<Point> vertices2;
    FillPolygon2Vertices(vertices2);
    vector<Point> segment2;
    FillSegment2Vertices(segment2);

    GeometryFactory geometryFactory;
    const int idPolygon = geometryFactory.CreatePolygon(vertices2);
    const Polygon polygon = geometryFactory.GetPolygon(idPolygon);

    try
    {
        Intersector1D1D intersector1D1D;
        const int numIntersectionPoints = intersector1D1D.ComputeIntersection(polygon, segment2);
        EXPECT_EQ(numIntersectionPoints, 2);
        EXPECT_EQ(intersector1D1D.GetPolygonNumberVertices(polygon), 5);
        EXPECT_EQ(intersector1D1D.IntersectionPoint(0).X, polygon.polygonPoints[1].X);
        EXPECT_EQ(intersector1D1D.IntersectionPoint(0).Y, polygon.polygonPoints[1].Y);
        EXPECT_EQ(intersector1D1D.IntersectionPoint(1).X, 1.2000e+00);
        EXPECT_EQ(intersector1D1D.IntersectionPoint(1).Y, 2.8000e+00);
    }

    catch (const exception& exception)
    {
      FAIL();
    }

}

TEST(TestComputationalScience, TestCutPolygon2)
{
    vector<Point> vertices2;
    FillPolygon2Vertices(vertices2);
    vector<Point> segment2;
    FillSegment2Vertices(segment2);

    //create polygon and segment
    GeometryFactory geometryFactory;
    const int idPolygon = geometryFactory.CreatePolygon(vertices2);
    const Polygon polygon_to_cut = geometryFactory.GetPolygon(idPolygon);

    //compute the intersection between the polygon and the segment, and save the results
    Intersector1D1D intersector1D1D;
    const int numIntersectionPoints = intersector1D1D.ComputeIntersection(polygon_to_cut, segment2);
    vector<Point> intersectionPoints = intersector1D1D._intersectionPoints;

    try
    {
      WorkPolygon workPolygon;
      const int operationId = workPolygon.CutPolygon(polygon_to_cut,
                                                     segment2);
      EXPECT_EQ(operationId, 1);
      vector<Polygon> cuttedPolygons = workPolygon.GetCuttedPolygons(operationId);
      EXPECT_EQ(cuttedPolygons[1].Vertici[2], 3);
      EXPECT_EQ(cuttedPolygons[0].Vertici[1], 1);
      vector<Point> newPoints = workPolygon.GetNewPoints(operationId);
      EXPECT_EQ(newPoints[2].X, vertices2[2].X);
      EXPECT_EQ(newPoints[2].Y, vertices2[2].Y);
      EXPECT_EQ(newPoints[5].X, intersectionPoints[1].X);
      EXPECT_EQ(newPoints[5].Y, intersectionPoints[1].Y);
      EXPECT_EQ(newPoints[7].X, segment2[1].X);
      EXPECT_EQ(newPoints[7].Y, segment2[1].Y);
      EXPECT_EQ(vertices2[1].X, intersectionPoints[0].X);
      EXPECT_EQ(vertices2[1].Y, intersectionPoints[0].Y);
    }

    catch (const exception& exception)
    {
      FAIL();
    }

    try
    {
      WorkPolygon workPolygon;
      workPolygon.GetCuttedPolygons(2);
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("CuttedPolygon not found"));
    }

    try
    {
      WorkPolygon workPolygon;
      workPolygon.GetNewPoints(2);
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Polygon not cutted"));
    }

}

  void FillPolygon3Vertices(vector<Point>& vertices)
  {
    vertices.push_back(Point(1.5000e+00, 1.0000e+00));
    vertices.push_back(Point(5.6000e-00, 1.5000e+00));
    vertices.push_back(Point(5.5000e-00, 4.8000e+00));
    vertices.push_back(Point(4.0000e+00, 6.2000e+00));
    vertices.push_back(Point(3.2000e+00, 4.2000e+00));
    vertices.push_back(Point(1.0000e+00, 4.0000e+00));
  }

  void FillSegment3Vertices(vector<Point>& segment)
  {
      segment.push_back(Point(2.0000e+00, 3.7000e+00));
      segment.push_back(Point(4.1000e+00, 5.9000e+00));
  }

  TEST(TestComputationalScience, TestCreatePolygon3)
  {
    vector<Point> vertices3;
    FillPolygon3Vertices(vertices3);

    GeometryFactory geometryFactory;

    try
    {
      EXPECT_EQ(geometryFactory.CreatePolygon(vertices3), 1);

      const int idPolygon = geometryFactory.CreatePolygon(vertices3);
      const Polygon polygon = geometryFactory.GetPolygon(idPolygon);
      EXPECT_EQ(polygon.polygonPoints[1].X, vertices3[1].X);
      EXPECT_EQ(polygon.polygonPoints[1].Y, vertices3[1].Y);
      EXPECT_EQ(polygon.Vertici[0], 0);


      EXPECT_EQ(geometryFactory.GetPolygonNumberVertices(1), 6);

      const Point& vertex = geometryFactory.GetPolygonVertex(1, 2);
      EXPECT_EQ(vertex.X, vertices3[2].X);
      EXPECT_EQ(vertex.Y, vertices3[2].Y);

    }
    catch (const exception& exception)
    {
      FAIL();
    }

    try
    {
      geometryFactory.GetPolygon(12);
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Polygon not found"));
    }

    try
    {
      geometryFactory.GetPolygonVertex(12, 2);
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Polygon not found"));
    }

    try
    {
      geometryFactory.GetPolygonVertex(1, 17);
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Vertex not found"));
    }
}

  TEST(TestComputationalScience, TestComputeIntersection3)
  {
      vector<Point> vertices3;
      FillPolygon3Vertices(vertices3);
      vector<Point> segment3;
      FillSegment3Vertices(segment3);

      GeometryFactory geometryFactory;
      const int idPolygon = geometryFactory.CreatePolygon(vertices3);
      const Polygon polygon = geometryFactory.GetPolygon(idPolygon);

      try
      {
          Intersector1D1D intersector1D1D;
          const int numIntersectionPoints = intersector1D1D.ComputeIntersection(polygon, segment3);
          EXPECT_EQ(numIntersectionPoints, 4);
          EXPECT_EQ(intersector1D1D.GetPolygonNumberVertices(polygon), 6);
          EXPECT_EQ(intersector1D1D.IntersectionPoint(0).X, 4.204326923076923);
          EXPECT_EQ(intersector1D1D.IntersectionPoint(0).Y, 6.009294871794872);
          EXPECT_EQ(intersector1D1D.IntersectionPoint(1).X, 3.7213114754098364);
          EXPECT_EQ(intersector1D1D.IntersectionPoint(1).Y, 5.503278688524591);
          EXPECT_EQ(intersector1D1D.IntersectionPoint(2).X, 2.408597285067873);
          EXPECT_EQ(intersector1D1D.IntersectionPoint(2).Y, 4.128054298642534);
          EXPECT_EQ(intersector1D1D.IntersectionPoint(3).X, 1.1912162162162163);
          EXPECT_EQ(intersector1D1D.IntersectionPoint(3).Y, 2.8527027027027025);
      }

      catch (const exception& exception)
      {
        FAIL();
      }
  }

  TEST(TestComputationalScience, TestCutPolygon3)
  {
      vector<Point> vertices3;
      FillPolygon3Vertices(vertices3);
      vector<Point> segment3;
      FillSegment3Vertices(segment3);

      //create polygon and segment
      GeometryFactory geometryFactory;
      const int idPolygon = geometryFactory.CreatePolygon(vertices3);
      const Polygon polygon_to_cut = geometryFactory.GetPolygon(idPolygon);

      //compute the intersection between the polygon and the segment, and save the results
      Intersector1D1D intersector1D1D;
      const int numIntersectionPoints = intersector1D1D.ComputeIntersection(polygon_to_cut, segment3);
      vector<Point> intersectionPoints = intersector1D1D._intersectionPoints;

      try
      {
        WorkPolygon workPolygon;
        const int operationId = workPolygon.CutPolygon(polygon_to_cut,
                                                       segment3);
        EXPECT_EQ(operationId, 1);
        vector<Polygon> cuttedPolygons = workPolygon.GetCuttedPolygons(operationId);
        EXPECT_EQ(cuttedPolygons[1].Vertici[2], 10);
        EXPECT_EQ(cuttedPolygons[0].Vertici[8], 7);
        EXPECT_EQ(cuttedPolygons[2].Vertici[3], 8);
        vector<Point> newPoints = workPolygon.GetNewPoints(operationId);
        EXPECT_EQ(newPoints[2].X, vertices3[2].X);
        EXPECT_EQ(newPoints[2].Y, vertices3[2].Y);
        EXPECT_EQ(newPoints[6].X, intersectionPoints[3].X);
        EXPECT_EQ(newPoints[6].Y, intersectionPoints[3].Y);
        EXPECT_EQ(newPoints[8].X, intersectionPoints[2].X);
        EXPECT_EQ(newPoints[8].Y, intersectionPoints[2].Y);
        EXPECT_EQ(newPoints[10].X, segment3[1].X);
        EXPECT_EQ(newPoints[10].Y, segment3[1].Y);
      }

      catch (const exception& exception)
      {
        FAIL();
      }

      try
      {
        WorkPolygon workPolygon;
        workPolygon.GetCuttedPolygons(2);
        FAIL();
      }
      catch (const exception& exception)
      {
        EXPECT_THAT(std::string(exception.what()), Eq("CuttedPolygon not found"));
      }

      try
      {
        WorkPolygon workPolygon;
        workPolygon.GetNewPoints(2);
        FAIL();
      }
      catch (const exception& exception)
      {
        EXPECT_THAT(std::string(exception.what()), Eq("Polygon not cutted"));
      }

  }

void FillPolygonDAuriaVertices(vector<Point>& vertices)
 {
   vertices.push_back(Point(2.0000e+00, -2.0000e+00));
   vertices.push_back(Point(0.0000e-00, -1.0000e+00));
   vertices.push_back(Point(3.0000e-00, 1.0000e+00));
   vertices.push_back(Point(0.0000e+00, 2.0000e+00));
   vertices.push_back(Point(3.0000e+00, 2.0000e+00));
   vertices.push_back(Point(3.0000e+00, 3.0000e+00));
   vertices.push_back(Point(-1.0000e+00, 3.0000e+00));
   vertices.push_back(Point(-3.0000e+00, 1.0000e+00));
   vertices.push_back(Point(0.0000e+00, 0.0000e+00));
   vertices.push_back(Point(-3.0000e+00, -2.0000e+00));
 }

 void FillSegment1DAuriaVertices(vector<Point>& segment)
 {
     segment.push_back(Point(0.0000e+00, -3.0000e+00));
     segment.push_back(Point(0.0000e+00, 4.0000e+00));
 }

 void FillSegment2DAuriaVertices(vector<Point>& segment)
 {
     segment.push_back(Point(-4.0000e+00, -4.0000e+00));
     segment.push_back(Point(4.0000e+00, 4.0000e+00));
 }

 TEST(TestComputationalScience, TestCreateDAuriaPolygon)
 {
   vector<Point> verticesDA;
   FillPolygonDAuriaVertices(verticesDA);

   GeometryFactory geometryFactory;

   try
   {
     EXPECT_EQ(geometryFactory.CreatePolygon(verticesDA), 1);

     const int idPolygon = geometryFactory.CreatePolygon(verticesDA);
     const Polygon polygon = geometryFactory.GetPolygon(idPolygon);
     EXPECT_EQ(polygon.polygonPoints[1].X, verticesDA[1].X);
     EXPECT_EQ(polygon.polygonPoints[1].Y, verticesDA[1].Y);
     EXPECT_EQ(polygon.Vertici[0], 0);


     EXPECT_EQ(geometryFactory.GetPolygonNumberVertices(1), 10);

     const Point& vertex = geometryFactory.GetPolygonVertex(1, 2);
     EXPECT_EQ(vertex.X, verticesDA[2].X);
     EXPECT_EQ(vertex.Y, verticesDA[2].Y);

   }
   catch (const exception& exception)
   {
     FAIL();
   }

   try
   {
     geometryFactory.GetPolygon(12);
     FAIL();
   }
   catch (const exception& exception)
   {
     EXPECT_THAT(std::string(exception.what()), Eq("Polygon not found"));
   }

   try
   {
     geometryFactory.GetPolygonVertex(12, 2);
     FAIL();
   }
   catch (const exception& exception)
   {
     EXPECT_THAT(std::string(exception.what()), Eq("Polygon not found"));
   }

   try
   {
     geometryFactory.GetPolygonVertex(1, 17);
     FAIL();
   }
   catch (const exception& exception)
   {
     EXPECT_THAT(std::string(exception.what()), Eq("Vertex not found"));
   }
 }

 TEST(TestComputationalScience, TestComputeIntersectionDAuria)
 {
     vector<Point> verticesDA;
     FillPolygonDAuriaVertices(verticesDA);
     vector<Point> segmentDA1, segmentDA2;
     FillSegment1DAuriaVertices(segmentDA1);
     FillSegment2DAuriaVertices(segmentDA2);

     GeometryFactory geometryFactory;
     const int idPolygon = geometryFactory.CreatePolygon(verticesDA);
     const Polygon polygon = geometryFactory.GetPolygon(idPolygon);

     try
     {
         Intersector1D1D intersector1D1D;
         const int numIntersectionPoints = intersector1D1D.ComputeIntersection(polygon, segmentDA1);
         EXPECT_EQ(numIntersectionPoints, 5);
         EXPECT_EQ(intersector1D1D.GetPolygonNumberVertices(polygon), 10);
         EXPECT_EQ(intersector1D1D.IntersectionPoint(0).X, polygon.polygonPoints[1].X);
         EXPECT_EQ(intersector1D1D.IntersectionPoint(0).Y, polygon.polygonPoints[1].Y);
         EXPECT_EQ(intersector1D1D.IntersectionPoint(1).X, polygon.polygonPoints[3].X);
         EXPECT_EQ(intersector1D1D.IntersectionPoint(1).Y, polygon.polygonPoints[3].Y);
         EXPECT_EQ(intersector1D1D.IntersectionPoint(2).X, 0.0000e+00);
         EXPECT_EQ(intersector1D1D.IntersectionPoint(2).Y, 3.0000e+00);
         EXPECT_EQ(intersector1D1D.IntersectionPoint(3).X, polygon.polygonPoints[8].X);
         EXPECT_EQ(intersector1D1D.IntersectionPoint(3).Y, polygon.polygonPoints[8].Y);
         EXPECT_EQ(intersector1D1D.IntersectionPoint(4).X, 0.0000e+00);
         EXPECT_EQ(intersector1D1D.IntersectionPoint(4).Y, -2.0000e+00);
     }

     catch (const exception& exception)
     {
       FAIL();
     }

     try
     {
         Intersector1D1D intersector1D1D;
         const int numIntersectionPoints = intersector1D1D.ComputeIntersection(polygon, segmentDA2);
         EXPECT_EQ(numIntersectionPoints, 5);
         EXPECT_EQ(intersector1D1D.GetPolygonNumberVertices(polygon), 10);
         EXPECT_EQ(intersector1D1D.IntersectionPoint(0).X, 1.5000e+00);
         EXPECT_EQ(intersector1D1D.IntersectionPoint(0).Y, 1.5000e+00);
         EXPECT_EQ(intersector1D1D.IntersectionPoint(1).X, 2.0000e+00);
         EXPECT_EQ(intersector1D1D.IntersectionPoint(1).Y, 2.0000e+00);
         EXPECT_EQ(intersector1D1D.IntersectionPoint(2).X, polygon.polygonPoints[5].X);
         EXPECT_EQ(intersector1D1D.IntersectionPoint(2).Y, polygon.polygonPoints[5].Y);
         EXPECT_EQ(intersector1D1D.IntersectionPoint(3).X, polygon.polygonPoints[8].X);
         EXPECT_EQ(intersector1D1D.IntersectionPoint(3).Y, polygon.polygonPoints[8].Y);
         EXPECT_EQ(intersector1D1D.IntersectionPoint(4).X, -2.0000e+00);
         EXPECT_EQ(intersector1D1D.IntersectionPoint(4).Y, -2.0000e+00);
     }

     catch (const exception& exception)
     {
       FAIL();
     }

 }

 TEST(TestComputationalScience, TestCutDAuriaPolygon)
 {
     vector<Point> verticesDA;
     FillPolygonDAuriaVertices(verticesDA);
     vector<Point> segmentDA1, segmentDA2;
     FillSegment1DAuriaVertices(segmentDA1);
     FillSegment2DAuriaVertices(segmentDA2);

     //create polygon and segment
     GeometryFactory geometryFactory;
     const int idPolygon = geometryFactory.CreatePolygon(verticesDA);
     const Polygon polygon_to_cut = geometryFactory.GetPolygon(idPolygon);

     try
     {
       //compute the intersection between the polygon and the segment, and save the results
       Intersector1D1D intersector1D1D;
       const int numIntersectionPoints_1 = intersector1D1D.ComputeIntersection(polygon_to_cut, segmentDA1);
       vector<Point> intersectionPoints_1 = intersector1D1D._intersectionPoints;
       WorkPolygon workPolygon;
       const int operationId = workPolygon.CutPolygon(polygon_to_cut,
                                                      segmentDA1);
       EXPECT_EQ(operationId, 1);
       vector<Polygon> cuttedPolygons = workPolygon.GetCuttedPolygons(operationId);
       EXPECT_EQ(cuttedPolygons[1].Vertici[1], 2);
       EXPECT_EQ(cuttedPolygons[0].Vertici[2], 10);
       EXPECT_EQ(cuttedPolygons[2].Vertici[0], 3);
       EXPECT_EQ(cuttedPolygons[3].Vertici[4], 11);
       EXPECT_EQ(cuttedPolygons[4].Vertici[3], 1);
       vector<Point> newPoints = workPolygon.GetNewPoints(operationId);
       EXPECT_EQ(newPoints[2].X, verticesDA[2].X);
       EXPECT_EQ(newPoints[2].Y, verticesDA[2].Y);
       EXPECT_EQ(newPoints[6].X, verticesDA[6].X);
       EXPECT_EQ(newPoints[6].Y, verticesDA[6].Y);
       EXPECT_EQ(newPoints[8].X, intersectionPoints_1[3].X);
       EXPECT_EQ(newPoints[8].Y, intersectionPoints_1[3].Y);
       EXPECT_EQ(newPoints[10].X, intersectionPoints_1[4].X);
       EXPECT_EQ(newPoints[10].Y, intersectionPoints_1[4].Y);
     }

     catch (const exception& exception)
     {
       FAIL();
     }

     try
     {
       WorkPolygon workPolygon;
       workPolygon.GetCuttedPolygons(2);
       FAIL();
     }

     catch (const exception& exception)
     {
       EXPECT_THAT(std::string(exception.what()), Eq("CuttedPolygon not found"));
     }

     try
     {
       WorkPolygon workPolygon;
       workPolygon.GetNewPoints(2);
       FAIL();
     }

     catch (const exception& exception)
     {
       EXPECT_THAT(std::string(exception.what()), Eq("Polygon not cutted"));
     }

     try
     {
       //compute the intersection between the polygon and the segment, and save the results
       Intersector1D1D intersector1D1D;
       const int numIntersectionPoints_2 = intersector1D1D.ComputeIntersection(polygon_to_cut, segmentDA2);
       vector<Point> intersectionPoints_2 = intersector1D1D._intersectionPoints;
       WorkPolygon workPolygon;
       const int operationId = workPolygon.CutPolygon(polygon_to_cut,
                                                      segmentDA2);
       EXPECT_EQ(operationId, 1);
       vector<Polygon> cuttedPolygons = workPolygon.GetCuttedPolygons(operationId);
       EXPECT_EQ(cuttedPolygons[0].Vertici[2], 2);
       EXPECT_EQ(cuttedPolygons[1].Vertici[5], 8);
       EXPECT_EQ(cuttedPolygons[2].Vertici[1], 5);
       EXPECT_EQ(cuttedPolygons[3].Vertici[2], 10);
       vector<Point> newPoints = workPolygon.GetNewPoints(operationId);
       EXPECT_EQ(newPoints[1].X, verticesDA[1].X);
       EXPECT_EQ(newPoints[1].Y, verticesDA[1].Y);
       EXPECT_EQ(newPoints[7].X, verticesDA[7].X);
       EXPECT_EQ(newPoints[7].Y, verticesDA[7].Y);
       EXPECT_EQ(newPoints[5].X, intersectionPoints_2[2].X);
       EXPECT_EQ(newPoints[5].Y, intersectionPoints_2[2].Y);
       EXPECT_EQ(newPoints[10].X, intersectionPoints_2[4].X);
       EXPECT_EQ(newPoints[10].Y, intersectionPoints_2[4].Y);

     }

     catch (const exception& exception)
     {
        FAIL();
     }

     try
     {
       WorkPolygon workPolygon;
       workPolygon.GetCuttedPolygons(2);
       FAIL();
     }

     catch (const exception& exception)
     {
       EXPECT_THAT(std::string(exception.what()), Eq("CuttedPolygon not found"));
     }

     try
     {
       WorkPolygon workPolygon;
       workPolygon.GetNewPoints(2);
       FAIL();
     }

     catch (const exception& exception)
     {
       EXPECT_THAT(std::string(exception.what()), Eq("Polygon not cutted"));
     }
 }

void FillConvexPolygonVertices(vector<Point>& vertices)
{
  vertices.push_back(Point(2.0000e+00, 2.0000e+00));
  vertices.push_back(Point(5.0000e-00, 1.0000e+00));
  vertices.push_back(Point(6.0000e-00, 4.0000e+00));
  vertices.push_back(Point(4.0000e+00, 7.0000e+00));
  vertices.push_back(Point(2.0000e+00, 6.0000e+00));
  vertices.push_back(Point(1.0000e+00, 4.0000e+00));
}

void FillConvexSegmentVertices(vector<Point>& segment)
{
    segment.push_back(Point(3.0000e+00, 0.0000e+00));
    segment.push_back(Point(5.0000e+00, 4.0000e+00));
}

TEST(TestComputationalScience, TestCreateConvexPolygon)
{
  vector<Point> verticesC;
  FillConvexPolygonVertices(verticesC);

  GeometryFactory geometryFactory;

  try
  {
    EXPECT_EQ(geometryFactory.CreatePolygon(verticesC), 1);

    const int idPolygon = geometryFactory.CreatePolygon(verticesC);
    const Polygon polygon = geometryFactory.GetPolygon(idPolygon);
    EXPECT_EQ(polygon.polygonPoints[1].X, verticesC[1].X);
    EXPECT_EQ(polygon.polygonPoints[1].Y, verticesC[1].Y);
    EXPECT_EQ(polygon.Vertici[0], 0);


    EXPECT_EQ(geometryFactory.GetPolygonNumberVertices(1), 6);

    const Point& vertex = geometryFactory.GetPolygonVertex(1, 2);
    EXPECT_EQ(vertex.X, verticesC[2].X);
    EXPECT_EQ(vertex.Y, verticesC[2].Y);

  }
  catch (const exception& exception)
  {
    FAIL();
  }

  try
  {
    geometryFactory.GetPolygon(12);
    FAIL();
  }
  catch (const exception& exception)
  {
    EXPECT_THAT(std::string(exception.what()), Eq("Polygon not found"));
  }

  try
  {
    geometryFactory.GetPolygonVertex(12, 2);
    FAIL();
  }
  catch (const exception& exception)
  {
    EXPECT_THAT(std::string(exception.what()), Eq("Polygon not found"));
  }

  try
  {
    geometryFactory.GetPolygonVertex(1, 17);
    FAIL();
  }
  catch (const exception& exception)
  {
    EXPECT_THAT(std::string(exception.what()), Eq("Vertex not found"));
  }
}

TEST(TestComputationalScience, TestComputeIntersectionConvexPolygon)
{
    vector<Point> verticesC;
    FillConvexPolygonVertices(verticesC);
    vector<Point> segmentC;
    FillConvexSegmentVertices(segmentC);

    GeometryFactory geometryFactory;
    const int idPolygon = geometryFactory.CreatePolygon(verticesC);
    const Polygon polygon = geometryFactory.GetPolygon(idPolygon);

    try
    {
        Intersector1D1D intersector1D1D;
        const int numIntersectionPoints = intersector1D1D.ComputeIntersection(polygon, segmentC);
        EXPECT_EQ(numIntersectionPoints, 2);
        EXPECT_EQ(intersector1D1D.GetPolygonNumberVertices(polygon), 6);
        EXPECT_EQ(intersector1D1D.IntersectionPoint(0).X, 3.7142857142857144);
        EXPECT_EQ(intersector1D1D.IntersectionPoint(0).Y, 1.4285714285714286);
        EXPECT_EQ(intersector1D1D.IntersectionPoint(1).X, 5.428571428571429);
        EXPECT_EQ(intersector1D1D.IntersectionPoint(1).Y, 4.857142857142857);
    }

    catch (const exception& exception)
    {
      FAIL();
    }

}

TEST(TestComputationalScience, TestCutConvexPolygon)
{
    vector<Point> verticesC;
    FillConvexPolygonVertices(verticesC);
    vector<Point> segmentC;
    FillConvexSegmentVertices(segmentC);

    //create polygon and segment
    GeometryFactory geometryFactory;
    const int idPolygon = geometryFactory.CreatePolygon(verticesC);
    const Polygon polygon_to_cut = geometryFactory.GetPolygon(idPolygon);

    //compute the intersection between the polygon and the segment, and save the results
    Intersector1D1D intersector1D1D;
    const int numIntersectionPoints = intersector1D1D.ComputeIntersection(polygon_to_cut, segmentC);
    vector<Point> intersectionPoints = intersector1D1D._intersectionPoints;

    try
    {
      WorkPolygon workPolygon;
      const int operationId = workPolygon.CutPolygon(polygon_to_cut,
                                                     segmentC);
      EXPECT_EQ(operationId, 1);
      vector<Polygon> cuttedPolygons = workPolygon.GetCuttedPolygons(operationId);
      EXPECT_EQ(cuttedPolygons[1].Vertici[3], 7);
      EXPECT_EQ(cuttedPolygons[0].Vertici[4], 3);
      vector<Point> newPoints = workPolygon.GetNewPoints(operationId);
      EXPECT_EQ(newPoints[3].X, verticesC[3].X);
      EXPECT_EQ(newPoints[3].Y, verticesC[3].Y);
      EXPECT_EQ(newPoints[6].X, intersectionPoints[0].X);
      EXPECT_EQ(newPoints[6].Y, intersectionPoints[0].Y);
      EXPECT_EQ(newPoints[7].X, segmentC[1].X);
      EXPECT_EQ(newPoints[7].Y, segmentC[1].Y);
    }

    catch (const exception& exception)
    {
      FAIL();
    }

    try
    {
      WorkPolygon workPolygon;
      workPolygon.GetCuttedPolygons(2);
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("CuttedPolygon not found"));
    }

    try
    {
      WorkPolygon workPolygon;
      workPolygon.GetNewPoints(2);
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Polygon not cutted"));
    }

}

}

#endif // __TEST_EMPTYCLASS_H
