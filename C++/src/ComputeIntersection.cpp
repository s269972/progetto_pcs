#include "ComputeIntersection.hpp"

namespace ComputationalScienceLibrary {

Intersector1D1D::Intersector1D1D()
{
    _toleranceParallelism = 1.0E-7;
    _toleranceIntersection = 1.0E-7;
}

int Intersector1D1D::ComputeIntersection(Polygon polygon, vector<Point> segment)
{
    //Variabili
    int numVertices = GetPolygonNumberVertices(polygon);
    bool intersection, found;
    Point IntersectionPoint;
    Vector2d resultParametricCoordinates;

    //Crea il segmento che "intersecherà" e "taglierà" il poligono nello spazio bidimiensionale
    //facendo la differenza delle coordinate dei due suoi punti estremi,
    //le due coordinate del segmento vengono poi inserite nella prima colonna della matrice "_matrixTangentVector"
    //come tipo "Vector2d" della libreria "Eigen"
    //infine viene salvato il primo estremo del segmento nell'attributo "_originFirstSegment"
    SetFirstSegment(segment[0], segment[1]);

    //calcola l'intersezione tra il segmento tagliante e ogni lato del poligono passato in input alla funzione
    for(int i = 0; i < numVertices; i++)
    {
        resultParametricCoordinates.setZero(2);

        //Esattamente come per il segmento intersecante, ma questa volta il segmento ottenuto è il lato del poligono
        //corrispondente alla posizione corrente del ciclo for.
        //Esso viene salvato nella seconda colonna della matrice "_matrixTangentVector"
        //viene inoltre salvato nell'attributo "_rightHandSide" il vettore che congiunge
        //il primo estremo del segmento tagliante con il primo estremo del lato corrente
        SetSecondSegment(polygon.polygonPoints[i], polygon.polygonPoints[(i+1) % numVertices]);

        intersection = false;
        found = false;
        Type type;
        Position positionIntersectionFirstEdge, positionIntersectionSecondEdge;

        double parallelism = _matrixTangentVector.determinant();
        double check = _toleranceParallelism * _toleranceParallelism * _matrixTangentVector.col(0).squaredNorm() *  _matrixTangentVector.col(1).squaredNorm();
        //Caso in cui il segmento tagliante e il lato corrente NON sono paralleli
        if(parallelism * parallelism >= check)
        {
            Matrix2d solverMatrix;
            solverMatrix << _matrixTangentVector(1,1), - _matrixTangentVector(0,1), - _matrixTangentVector(1,0), _matrixTangentVector(0,0);
            resultParametricCoordinates = solverMatrix * _rightHandSide;
            resultParametricCoordinates /= parallelism;
            //Caso in cui l'intersezione avvenga tra il prolungamento del segmento tagliante ed il lato corrente
            if (resultParametricCoordinates(1) > - _toleranceIntersection  && resultParametricCoordinates(1) - 1.0 < _toleranceIntersection)
            {
                type = IntersectionOnLine;
                intersection = true;
                //Intersezione incidente tra segmento tagliante e lato corrente
                if (resultParametricCoordinates(0) > - _toleranceIntersection  && resultParametricCoordinates(0) - 1.0 < _toleranceIntersection)
                    type = IntersectionOnSegment;
            }
        }
        //Caso in cui il segmento tagliante e il lato corrente sono paralleli
        else
        {
            double parallelism2 = fabs(_matrixTangentVector(0,0) * _rightHandSide.y() - _rightHandSide.x() * _matrixTangentVector(1,0));

            double squaredNormFirstEdge = _matrixTangentVector.col(0).squaredNorm();
            double check2 = _toleranceParallelism * _toleranceParallelism * squaredNormFirstEdge * _rightHandSide.squaredNorm();
            //Caso in cui il segmento tagliante e il lato corrente appartengono alla stessa retta
            if( parallelism2 * parallelism2 <= check2 )
            {
                double tempNorm = 1.0/squaredNormFirstEdge;
                resultParametricCoordinates(0) = _matrixTangentVector.col(0).dot(_rightHandSide) * tempNorm ;
                resultParametricCoordinates(1) = resultParametricCoordinates(0) - _matrixTangentVector.col(0).dot(_matrixTangentVector.col(1)) * tempNorm;

                intersection = true;
                type = IntersectionParallelOnLine;

                //Caso in cui il segmento tagliante e il lato corrente si sovrappongono (anche solo in parte)
                if( (resultParametricCoordinates(0) > - _toleranceIntersection && resultParametricCoordinates(0) - 1.0 < _toleranceIntersection) ||
                    (resultParametricCoordinates(1) > - _toleranceIntersection && resultParametricCoordinates(1) - 1.0 < _toleranceIntersection)   )
                    type = IntersectionParallelOnSegment;
                else
                {
                    if( ( resultParametricCoordinates(0) < _toleranceIntersection && resultParametricCoordinates(1) - 1.0 > - _toleranceIntersection) )
                        type = IntersectionParallelOnSegment;
                }
            }
        }

        //Determina la posizione del punto di intersezione rispetto al segmento tagliante
        if(resultParametricCoordinates(0) < -_toleranceIntersection || resultParametricCoordinates(0) > 1.0 + _toleranceIntersection)
          positionIntersectionFirstEdge =  Outer;
        else if((resultParametricCoordinates(0) > -_toleranceIntersection) && (resultParametricCoordinates(0) < _toleranceIntersection))
        {
          resultParametricCoordinates(0) = 0.0;
          positionIntersectionFirstEdge = Begin;
        }
        else if ((resultParametricCoordinates(0) > 1.0 - _toleranceIntersection) && (resultParametricCoordinates(0) < 1.0 + _toleranceIntersection))
        {
          resultParametricCoordinates(0) = 1.0;
          positionIntersectionFirstEdge = End;
        }
        else
          positionIntersectionFirstEdge = Inner;

        //Determina la posizione del punto di intersezione rispetto al lato corrente
        if(resultParametricCoordinates(1) < - _toleranceIntersection || resultParametricCoordinates(1) > 1.0 + _toleranceIntersection)
          positionIntersectionSecondEdge =  Outer;
        else if((resultParametricCoordinates(1) > - _toleranceIntersection) && (resultParametricCoordinates(1) < _toleranceIntersection))
        {
          resultParametricCoordinates(1) = 0.0;
          positionIntersectionSecondEdge = Begin;
        }
        else if ((resultParametricCoordinates(1) > 1.0 - _toleranceIntersection) && (resultParametricCoordinates(1) < 1.0 + _toleranceIntersection))
        {
          resultParametricCoordinates(1) = 1.0;
          positionIntersectionSecondEdge = End;
        }
        else
          positionIntersectionSecondEdge = Inner;

        IntersectionPoint.X = segment[0].X + ((segment[1].X - segment[0].X) * resultParametricCoordinates(0));
        IntersectionPoint.Y = segment[0].Y + ((segment[1].Y - segment[0].Y) * resultParametricCoordinates(0));
        //Fa in modo che non venga inserita più volte una stessa intersezione
        //questo può succedere quando per esempio si ha un'intersezione che cade su un vertice del poligono
        //e l'algoritmo considera l'intersezione su entrambi i lati con quel vertice in comune
        for (int it = 0; it < _intersectionPoints.size(); it++)
        {
            if((_intersectionPoints[it].X == IntersectionPoint.X) && (_intersectionPoints[it].Y == IntersectionPoint.Y))
            {
                found = true;
                break;
            }
        }
        //Se l'intersezione è stata trovata, si salva tutte le informazioni relative all'intersezione nei vari attributi della classe Intersector1D1D
        if (intersection == true && found == false)
        {
            _intersectionPoints.push_back(IntersectionPoint);
            _type.push_back(type);
            _positionIntersectionFirstEdge.push_back(positionIntersectionFirstEdge);
            _positionIntersectionSecondEdge.push_back(positionIntersectionSecondEdge);
            _everyResultParametricCoordinates.push_back(resultParametricCoordinates(0));
        }
    }
    return _intersectionPoints.size();
}

void Intersector1D1D::SetFirstSegment(Point origin, Point end)
{
    Vector2d Origin, End;
    Origin.x() = origin.X;
    Origin.y() = origin.Y;
    End.x() = end.X;
    End.y() = end.Y;
    _matrixTangentVector.col(0) = End - Origin;
    _originFirstSegment = Origin;
}

void Intersector1D1D::SetSecondSegment(Point origin, Point end)
{
    Vector2d Origin, End;
    Origin.x() = origin.X;
    Origin.y() = origin.Y;
    End.x() = end.X;
    End.y() = end.Y;
    _matrixTangentVector.col(1) = Origin - End;
    _rightHandSide = Origin - _originFirstSegment;
}

}
