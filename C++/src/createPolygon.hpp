#ifndef CREATEPOLYGON_H
#define CREATEPOLYGON_H

#include "DTO.hpp"

using namespace std;
using namespace Eigen;

namespace ComputationalScienceLibrary {

class IGeometryFactory {
  public:
    virtual int CreatePolygon(const vector<Point>& vertices) = 0;
    virtual const Polygon& GetPolygon(const int& polygonId) = 0;
    virtual int GetPolygonNumberVertices(const int& polygonId) = 0;
    virtual const Point& GetPolygonVertex(const int& polygonId,
                                          const int& vertexPosition) = 0;
};

class GeometryFactory : public IGeometryFactory {
  private:
    unordered_map<int, Polygon> _polygons;

  public:
    int CreatePolygon(const vector<Point>& vertices);
    const Polygon& GetPolygon(const int& polygonId);
    int GetPolygonNumberVertices(const int& polygonId);
    const Point& GetPolygonVertex(const int& polygonId,
                                  const int& vertexPosition);
};

}

#endif // EMPTYCLASS_H
