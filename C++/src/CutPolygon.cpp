#include "CutPolygon.hpp"
#include <vector>

namespace ComputationalScienceLibrary{

WorkPolygon::WorkPolygon()
{
    _toleranceContained = 1.0E-7;
}

int WorkPolygon::CutPolygon(Polygon polygon_to_cut,
                            vector<Point> segment)
{
    //Variabili:   
    double checkEnter = 0;
    bool first;    
    int i, j, e, k, u;
    vector<Polygon> tempOperation;
    double prodScal;
    Vector2d vectorSegment, normalVectorEdge;
    vector<Point> tempNewPoint;
    Vector2d vectorEdge;

    int numVertices = polygon_to_cut.polygonPoints.size();
    int operationId = _operations.size() + 1;

    //Calcolo delle intersezioni e coordinate parametriche rispetto agli estremi del segmento
    Intersector1D1D intersector1D1D;
    intersector1D1D.ComputeIntersection(polygon_to_cut, segment);
    vector<Position> positionIntersectionSecondEdge = intersector1D1D._positionIntersectionSecondEdge;
    vector<Point> intersectionPoints = intersector1D1D._intersectionPoints;
    vector<double> parametricCoordinates = intersector1D1D._everyResultParametricCoordinates;

    //Con la funzione CreateVectorNewPoint vengono salvati in tempNewPoint i vertici del poligono da tagliare, le intersezioni e gli eventuali
    //estremi del segmento interni al poligono. Viene inoltre creato il vettore _newPoint che salva gli stessi punti, depurato però dei doppioni
    //(ad esempio se un'intersezione coincide con un vertice, questa viene salvata soltanto una volta)
    tempNewPoint = CreateVectorNewPoint(intersectionPoints, parametricCoordinates, polygon_to_cut, segment, operationId);

   //Case noIntersection
    if(intersectionPoints.size() <= 1)
        tempOperation.push_back(polygon_to_cut);
    else
    {
        //Calcolo del vettore direzione del segmento
        vectorSegment.x() = segment[1].X - segment[0].X;
        vectorSegment.y() = segment[1].Y - segment[0].Y;

        //In questo vettore salviamo i vertici che ancora non sono stati aggiunti ad alcun poligono
        vector<int> verticinonvisitati = polygon_to_cut.Vertici;

        while (verticinonvisitati.size() != 0)
        {
            Polygon tempPolygon;
            first = true;

            //k vertice da cui parte il tempPolygon
            k = verticinonvisitati[0];

            for(i = k; i < numVertices; i++) // i cicla sui vertici del poligono
            {
                //Nel caso in cui si ritorni al vertice di partenza del poligono, il ciclo for si chiude e viene pushato il tempPolygon
                if ((i % numVertices == k) && (first == false))
                    break;

                tempPolygon.Vertici.push_back(i);
                tempPolygon.polygonPoints.push_back(_newPoints[operationId][i]);
                first = false;
                //Ciclo che elimina il vertice appena aggiunto al poligono, dal vettore di vertici non visitati
                for(auto o = verticinonvisitati.begin(); o<verticinonvisitati.end(); o++)
                {
                    if (i == *o)
                    {
                        verticinonvisitati.erase(o);
                        break;
                    }
                }
                for(j=0; j < int(intersectionPoints.size()); j++)
                {
                    //j cicla sulle intersezioni e vede se appartengono al lato in analisi
                    checkEnter = Contained(intersectionPoints[j], polygon_to_cut.polygonPoints[i], polygon_to_cut.polygonPoints[(i+1) % numVertices]);
                    if((checkEnter < _toleranceContained && checkEnter > -_toleranceContained))
                    {
                        //Controllo se l'intersezione coincide con un vertice e in quel caso aggiungo tale vertice al tempPolygon, ignorando l'intersezione
                        if(positionIntersectionSecondEdge[j] == End || positionIntersectionSecondEdge[j] == Begin )
                        {
                            //Questo if serve a non aggiungere due volte il vertice, nel caso in cui si stia controllando l'intersezione che coincide con il vertice di partenza del tempPolygon
                            if(intersectionPoints[j].X != polygon_to_cut.polygonPoints[i].X || intersectionPoints[j].Y != polygon_to_cut.polygonPoints[i].Y)
                            {
                                //Cerca a quale vertice l'intersezione corrisponde
                                for(int v = 0; v < numVertices; v++)
                                {
                                    if(intersectionPoints[j].X == polygon_to_cut.polygonPoints[v].X && intersectionPoints[j].Y == polygon_to_cut.polygonPoints[v].Y)
                                    {
                                        tempPolygon.Vertici.push_back(v);
                                        tempPolygon.polygonPoints.push_back(tempNewPoint[v]);
                                        //Cerca a quale indice di tempNewPoint corrisponde l'intersezione
                                        //il valore di e serve per iniziare a scorrere la retta tagliante
                                        for(e=numVertices; e < int(tempNewPoint.size()); e++)
                                        {
                                           if ((tempNewPoint[e].X == intersectionPoints[j].X) && (tempNewPoint[e].Y == intersectionPoints[j].Y))
                                               break;
                                        }
                                        break;
                                    }
                                }
                            }
                            //Il ciclo su j viene interrotto quando il tempPolygon parte da un vertice coincidente con un'intersezione
                            //evitando così di scendere sulla retta passante per gli estremi del segmento (vedi poligono 2)
                            else                                
                                break;
                        }
                        else
                        {
                            // e: questo ciclo cerca a quale indice di vertice corrisponde l'intersezione nel tempNewPoint
                            //il valore di e serve per iniziare a scorrere la retta tagliante
                            for(e = numVertices; e < int(tempNewPoint.size()); e++)
                            {
                               if ((tempNewPoint[e].X == intersectionPoints[j].X) && (tempNewPoint[e].Y == intersectionPoints[j].Y))
                               {
                                   //Cerca a quale elemento di newPoint l'intersezione corrisponde
                                   for(u = 0; u < int(_newPoints[operationId].size()); u++)
                                   {
                                       if((tempNewPoint[e].X == _newPoints[operationId][u].X) && (tempNewPoint[e].Y == _newPoints[operationId][u].Y))
                                           break;
                                   }
                                   tempPolygon.Vertici.push_back(u);
                                   tempPolygon.polygonPoints.push_back(_newPoints[operationId][u]);
                                   break;
                               }
                            }
                        }

                        //Quando viene aggiunta un'intersezione al poligono, occorre percorrere la retta passante per il segmento fino all'intersezione successiva,
                        //aggiungendo gli eventuali punti interni del segmento

                        //Per trovare la normale al lato si fa una rotazione di 90° in senso orario del vettore direzione del lato (direzione uscente dal poligono)
                        normalVectorEdge.x() = tempNewPoint[i+1].Y - tempNewPoint[i].Y;
                        normalVectorEdge.y() = -(tempNewPoint[i+1].X - tempNewPoint[i].X);

                        prodScal = normalVectorEdge.dot(vectorSegment);

                        //Se il prodotto scalare tra normale al lato e segmento è minore di 0 significa che si deve percorrere il segmento verso sinistra
                        if (prodScal<0)
                        {
                            //Il metodo ClimbSegment aggiunge al tempPolygon tutti i vertici della retta passante per il segmento
                            //fino all'intersezione successiva, percorrendola nel verso delle coordinate parametriche decrescenti
                            i = ClimbSegment(operationId, e, tempNewPoint, polygon_to_cut, vectorSegment, tempPolygon);

                        }

                        //Se il prodotto scalare tra normale al lato e segmento è maggiore di 0 significa che si deve percorrere il segmento verso destra
                        else
                        {
                            //Il metodo DescentSegment aggiunge al tempPolygon tutti i vertici della retta passante per il segmento
                            //fino all'intersezione successiva, percorrendola nel verso delle coordinate parametriche crescenti
                            i = DescentSegment(operationId, e, tempNewPoint, polygon_to_cut, vectorSegment, tempPolygon);
                        }
                        break;
                    }
                }
            }
        //push poligono quando ritorno alla posizione di partenza del for
        tempOperation.push_back(tempPolygon);
        }
    }

    _operations.insert(pair<int, vector<Polygon>>(operationId, tempOperation));
    return operationId;
}

vector<Polygon> &WorkPolygon::GetCuttedPolygons(int operationId)
{
    const auto& operationIterator = _operations.find(operationId);

    if (operationIterator == _operations.end())
      throw runtime_error("CuttedPolygon not found");

    return operationIterator->second;
}

vector<Point>& WorkPolygon::GetNewPoints(int operationId)
{
    const auto& pointsIterator = _newPoints.find(operationId);

    if (pointsIterator == _newPoints.end())
      throw runtime_error("Polygon not cutted");

    return pointsIterator->second;
}

double WorkPolygon::Contained(Point p_2, Point p_0, Point p_1)
{
    //Calcola il prodotto vettoriale tra vettore che congiunge i due estremi del lato
    //e vettore che congiunge primo estremo e intersezione.
    Point P_0, P_1, V, W;
    double Z;
    V.X = p_1.X - p_0.X;
    V.Y = p_1.Y - p_0.Y;
    W.X = p_2.X - p_0.X;
    W.Y = p_2.Y - p_0.Y;
    Z = (V.X * W.Y) - (V.Y * W.X);
    return Z;
}

vector<Point> WorkPolygon::CreateVectorNewPoint(vector<Point> intersectionPoints, vector<double> parametricCoordinates, const Polygon& polygon, vector<Point> segment, int operationId)
{
    vector<int> NOTCONTAINEDDOWN, NOTCONTAINEDUP, NOTCONTAINED;
    int notContainedDown, notContainedUp;
    vector<Point> pushPoint, tempNemPoint;

    vector<Point> extraPoints;
    vector<double> extraParametricCoordinates;
    vector<double> SegmentParametricCoordinates;
    vector<Point> tempNewPoint;

    SegmentParametricCoordinates.push_back(0);
    SegmentParametricCoordinates.push_back(1);


    //Controlla se gli estremi del segmento sono interni al poligono
    //e si salva l'informazione in un vettore di interi,
    //lo 0 corrisponde a "è contenuto" mentre l' 1 a "NON è contenuto"
    NOTCONTAINED.resize(2);
    NOTCONTAINED[0] = 0;
    NOTCONTAINED[1] = 0;
    NOTCONTAINEDDOWN.resize(2);
    NOTCONTAINEDDOWN[0] = 0;
    NOTCONTAINEDDOWN[1] = 0;
    NOTCONTAINEDUP.resize(2);
    NOTCONTAINEDUP[0] = 0;
    NOTCONTAINEDUP[1] = 0;

    //Controlla per ognuno dei due punti del segmento
    //se la loro coordinata parametrica, rispetto alla retta a cui il segmento appartiene,
    //è minore o maggiore delle coordinate parametriche sempre relative alla stessa retta
    for(int j = 0; j < 2; j++)
    {
        notContainedDown = 0;
        notContainedUp = 0;
        for(int u = 0; u < int(intersectionPoints.size()); u++)
        {
            if(j < parametricCoordinates[u])
                notContainedDown++;
        }
        if(notContainedDown == int(intersectionPoints.size()))
            NOTCONTAINEDDOWN[j] = 1;
        for(int u = 0; u < int(intersectionPoints.size()); u++)
        {
            if(j > parametricCoordinates[u])
                notContainedUp++;
        }
        if(notContainedUp == int(intersectionPoints.size()))
            NOTCONTAINEDUP[j] = 1;
    }
    //Se uno dei due casi, per ciascuno dei due punti del segmento, è verificato,
    //significa che il punto non è contenuto nel segmento
    //(o perchè è "al di sotto" (DOWN) del segmento, o perchè è "al di sopra" (UP))
    for(int j = 0; j < 2; j++)
        if(NOTCONTAINEDDOWN[j] == 1 || NOTCONTAINEDUP[j] == 1)
            NOTCONTAINED[j] = 1;

    //Riempiamo i vettori extraPoint e extraParametricCoordinates con i punti
    //e le rispettive coordinate parametriche degli estremi del segmento interni al poligono
    //e dei punti di intersezione
    for(int e=0; e < int(intersectionPoints.size() + segment.size()) ; e++)
    {
        if(e < int(segment.size()))
        {
            if(NOTCONTAINED[e]==0)
            {
                extraPoints.push_back(segment[e]);
                extraParametricCoordinates.push_back(SegmentParametricCoordinates[e]);
            }
        }
        else
        {
            extraPoints.push_back(intersectionPoints[e-segment.size()]);
            extraParametricCoordinates.push_back(parametricCoordinates[e-segment.size()]);
        }
    }

    //Ordino le coordinate parametriche e i punti del segmento interni in ordine crescente rispetto la loro coordinata parametrica sfruttando l'algoritmo bubblesort
    for (int i = 0; i < int(extraPoints.size()) -1; i++)
    {
        for (int j = 0; j < int(extraPoints.size()) - i - 1; j++)
        {
            if (extraParametricCoordinates[j] > extraParametricCoordinates[j+1])
            {
                swap(extraParametricCoordinates[j], extraParametricCoordinates[j+1]);
                swap(extraPoints[j],extraPoints[j+1]);
            }
        }
    }

    //Creiamo il vettore _newPoint che contiene i vertici del poligono, le intersezioni e gli eventuali estremi del segmento interni
    //La posizione che il punto ha in questo vettore corrisponde all'indice del vertice (attenzione perchè tempNewPoint contiene i doppi)
    tempNewPoint = polygon.polygonPoints;
    for (int t = 0; t < int(extraPoints.size());t++)
        tempNewPoint.push_back(extraPoints[t]);

    //vettore definitivo di newPoints depurato dai doppi

    for(int y=0; y < int(tempNewPoint.size()); y++)
    {
        pushPoint.push_back(tempNewPoint[y]);
        for(int w = 0; w < int(pushPoint.size())-1; w++)
        {
            if(tempNewPoint[y].X == pushPoint[w].X && tempNewPoint[y].Y == pushPoint[w].Y)
            {
                pushPoint.pop_back();
                break;
            }
        }
    }
    _newPoints.insert(pair<int, vector<Point>>(operationId, pushPoint));
    return tempNewPoint;
}

int WorkPolygon:: ClimbSegment(int operationId, int nInt, vector<Point> &tempNewPoint, const Polygon &polygon_to_cut, Vector2d vectorSegment, Polygon &tempPolygon)
{
    int t, u, z;
    int numVertices = polygon_to_cut.polygonPoints.size();
    double checkEnter;
    Vector2d vectorEdge;
    double checkLeft = 0;
    bool contenuto = false;

    //t cicla sui tempNewPoint
    for(t = nInt + 1; t < int(tempNewPoint.size()); t++)
    {
        //Trova a quale indice nel newPoint corrisponde il punto considerato del tempNewPoint
        for(u = 0; u < int(_newPoints[operationId].size()); u++)
        {
            if((tempNewPoint[t].X == _newPoints[operationId][u].X) && (tempNewPoint[t].Y == _newPoints[operationId][u].Y))
                break;
        }
        tempPolygon.Vertici.push_back(u);
        tempPolygon.polygonPoints.push_back(_newPoints[operationId][u]);

        //z cicla sui lati
        //Grazie al flag "contenuto" si interrompe il ciclo sulle intersezioni, quando una di queste appartiene ad un lato alla sinistra del verso di percorrenza del segmento
        for(z = 0; z < numVertices; z++)
        {
            checkEnter = Contained(tempNewPoint[t], polygon_to_cut.polygonPoints[z], polygon_to_cut.polygonPoints[(z+1) % numVertices]);
            if((checkEnter < _toleranceContained && checkEnter > -_toleranceContained))
            {
                if(u < numVertices) // se l'intersezione coincide con un vertice
                {
                    vectorEdge.x() = _newPoints[operationId][(u+1) % numVertices].X - _newPoints[operationId][u].X;
                    vectorEdge.y() = _newPoints[operationId][(u+1) % numVertices].Y - _newPoints[operationId][u].Y;
                    checkLeft = vectorSegment.x() * vectorEdge.y() - vectorSegment.y() * vectorEdge.x();

                    //Se il lato che parte da tale vertice risulta essere a destra del segmento, il ciclo che scorre il segmento e cerca altri punti di intersezione
                    //(ciclo con indice t) continua e "contenuto" rimane settato a false
                    if(checkLeft < 0)
                        break;

                    //Nel caso in cui invece il lato successivo sia alla sinistra, l'intersezione viene tolta dai vertici del tempPolygon,
                    //in quanto il vertice verrà aggiunto successivamente (nel ciclo principale dei lati, in indice i)
                    //Contenuto viene settato a true e si esce dal ciclo sui punti del segmento che tagliano il poligono (indice t)
                    else
                    {
                        tempPolygon.Vertici.pop_back();
                        tempPolygon.polygonPoints.pop_back();
                    }
                }
                contenuto = true;
                break;
            }
        }
        if(contenuto == true)
            break;
    }
    return z;
}

int WorkPolygon:: DescentSegment(int operationId, int nInt, vector<Point> &tempNewPoint, const Polygon &polygon_to_cut, Vector2d vectorSegment, Polygon &tempPolygon)
{
    int t, u, z;
    int numVertices = polygon_to_cut.polygonPoints.size();
    double checkEnter;
    Vector2d vectorEdge;
    double checkLeft = 0;
    bool contenuto = false;

    // t cicla sui punti da aggiungere controllando se appartengono al lato
    for(t = nInt - 1 ; t > numVertices - 1; t--)
    {
        //Trova a quale indice nel newPoint corrisponde il punto considerato del tempNewPoint
        for(u = 0; u < int(_newPoints[operationId].size()); u++)
        {
             if((tempNewPoint[t].X == _newPoints[operationId][u].X) && (tempNewPoint[t].Y == _newPoints[operationId][u].Y))
                 break;
         }
         tempPolygon.Vertici.push_back(u);
         tempPolygon.polygonPoints.push_back(_newPoints[operationId][u]);


        //z cicla sui lati
        //grazie al flag "contenuto" si interrompe il ciclo sulle intersezioni, quando una di queste appartiene ad un lato
        for(z = 0; z < numVertices; z++)
        {
            checkEnter = Contained(tempNewPoint[t], polygon_to_cut.polygonPoints[z], polygon_to_cut.polygonPoints[(z+1) % numVertices]);
            if((checkEnter < _toleranceContained && checkEnter > -_toleranceContained))// && type[i] != IntersectionParallelOnLine)
            {
                if(u < numVertices) // se l'intersezione coincide con un vertice
                {
                    vectorEdge.x() = _newPoints[operationId][(u+1) % numVertices].X - _newPoints[operationId][u].X;
                    vectorEdge.y() = _newPoints[operationId][(u+1) % numVertices].Y - _newPoints[operationId][u].Y;
                    checkLeft = -(vectorSegment.x()) * vectorEdge.y() - (-vectorSegment.y()) * vectorEdge.x();

                    //Se il lato che parte da tale vertice risulta essere a destra del segmento, il ciclo che scorre il segmento e cerca altri punti di intersezione
                    //(ciclo con indice t) continua e "contenuto" rimane settato a false
                    if(checkLeft < 0)
                        break;

                    //Nel caso in cui invece il lato successivo sia alla sinistra, l'intersezione viene tolta dai vertici del tempPolygon,
                    //in quanto il vertice verrà aggiunto successivamente (nel ciclo principale dei lati, in indice i)
                    //Contenuto viene settato a true e si esce dal ciclo sui punti del segmento che tagliano il poligono (indice t)
                    else
                    {
                        tempPolygon.Vertici.pop_back();
                        tempPolygon.polygonPoints.pop_back();
                    }
                }
                contenuto = true;
                break;
            }
        }
        if(contenuto == true)
            break;
    }
    return z;
}

}
