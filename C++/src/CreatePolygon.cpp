#include "createPolygon.hpp"

namespace ComputationalScienceLibrary {


int GeometryFactory::CreatePolygon(const vector<Point> &vertices)
{
  //Id del poligono creato
  int polygonId = _polygons.size() + 1;
  unsigned int numVertices = vertices.size();

  Polygon tempPolygon;

  //Riempe un poligono temporaneo con i punti passati in input
  //e con i vertici, che partono da 0
  for (unsigned int v = 0; v < numVertices; v++)
  {
      tempPolygon.polygonPoints.push_back(vertices[v]);
      tempPolygon.Vertici.push_back(v);
  }

  //Salva il poligono temporaneo creato nella mappa "_polygons", attributo della classe GeometryFactory
  _polygons.insert(pair<int, Polygon>(polygonId, tempPolygon));

  return polygonId;
}

const Polygon& GeometryFactory::GetPolygon(const int& polygonId)
{

  //Cerca il poligono da restituire all'interno della mappa
  //usando come chiave l'Id passato in input

  const auto& polygonIterator = _polygons.find(polygonId);

  if (polygonIterator == _polygons.end())
    throw runtime_error("Polygon not found");

  return polygonIterator->second;
}

int GeometryFactory::GetPolygonNumberVertices(const int& polygonId)
{

  //Cerca il poligono all'interno della mappa dopo avergli passato l'Id corrispondente,
  //dopodichè viene restituita la lunghezza del vettore di Vertici del poligono trovato

  const auto& polygonIterator = _polygons.find(polygonId);

  if (polygonIterator == _polygons.end())
    throw runtime_error("Polygon not found");

  const Polygon tempPolygon = polygonIterator->second;
  return tempPolygon.Vertici.size();
}

const Point& GeometryFactory::GetPolygonVertex(const int& polygonId, const int& vertexPosition)
{

  //Cerca il poligono all'interno della mappa dopo avergli passato l'Id corrispondente,
  //dopodichè vengono restituite le coordinate del punto alla posizione passata in input del poligono trovato

  const auto& polygonIterator = _polygons.find(polygonId);

  if (polygonIterator == _polygons.end())
    throw runtime_error("Polygon not found");

  const Polygon tempPolygon = polygonIterator->second;

  if ((unsigned int)vertexPosition >= tempPolygon.Vertici.size())
    throw runtime_error("Vertex not found");

  return tempPolygon.polygonPoints[vertexPosition];
}

}

