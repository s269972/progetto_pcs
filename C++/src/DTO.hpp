#ifndef DTO_H
#define DTO_H

#include <unordered_map>
#include <vector>
#include <iostream>
#include <cmath>
#include "Eigen"
#include <vector>
#include <list>
#include <unordered_map>

using namespace std;

namespace ComputationalScienceLibrary {

class Point {
  public:
    double X;
    double Y;
  public:
    Point(const double& x,
          const double& y) {X = x; Y = y; }
    Point() { X = 0.0; Y = 0.0; }
};

class Polygon {
  public:
    vector<Point> polygonPoints;
    vector<int> Vertici;
};

enum Type
{
    NoIntersection = 0,
    IntersectionOnLine = 1,
    IntersectionOnSegment = 2,
    IntersectionParallelOnLine = 3,
    IntersectionParallelOnSegment = 4
};
enum Position
{
    Begin = 0,
    Inner = 1,
    End = 2,
    Outer = 3
};

}

#endif // POLYGONCLASS_H
