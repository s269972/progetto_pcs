#ifndef COMPUTEINTERSECTION_HPP
#define COMPUTEINTERSECTION_HPP

#include "CreatePolygon.hpp"

namespace ComputationalScienceLibrary {

class IIntersector1D1D
{
  public:
    virtual int ComputeIntersection(Polygon polygon, vector<Point> segment) = 0;
    virtual void SetFirstSegment(Point origin, Point end) = 0;
    virtual void SetSecondSegment(Point origin, Point end) = 0;
    virtual int GetPolygonNumberVertices(Polygon polygon_to_cut) = 0;
    virtual Point IntersectionPoint(int i) = 0;
};

class Intersector1D1D : public IIntersector1D1D
{
  public:
    double _toleranceParallelism;
    double _toleranceIntersection;
    vector<double> _everyResultParametricCoordinates;
    Vector2d _originFirstSegment;
    Vector2d _rightHandSide;
    Matrix2d _matrixTangentVector;
    vector<Type> _type;
    vector<Position> _positionIntersectionFirstEdge, _positionIntersectionSecondEdge;
    vector<Point> _intersectionPoints;

  public:
    Intersector1D1D();
    int ComputeIntersection(Polygon polygon, vector<Point> segment);
    void SetFirstSegment(Point origin, Point end);
    void SetSecondSegment(Point origin, Point end);
    int GetPolygonNumberVertices(Polygon polygon_to_cut){return polygon_to_cut.polygonPoints.size();}
    Point IntersectionPoint(int i) {return _intersectionPoints[i];}

};
}
#endif // COMPUTEINTERSECTION_HPP
