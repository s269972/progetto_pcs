#ifndef CUTPOLYGON_H
#define CUTPOLYGON_H

#include "CreatePolygon.hpp"
#include "ComputeIntersection.hpp"

using namespace std;

namespace ComputationalScienceLibrary {

class IWorkPolygon {

    public:
        virtual int CutPolygon(Polygon polygon_to_cut,
                               vector<Point> segment) = 0;
        virtual vector<Polygon>& GetCuttedPolygons(int operationId) = 0;
        virtual vector<Point>& GetNewPoints(int operationId) = 0;
        virtual double Contained(Point p_2, Point p_0, Point p_1) = 0;
        virtual vector<Point> CreateVectorNewPoint(vector<Point> intersectionPoints, vector<double> parametricCoordinates, const Polygon& polygon, vector<Point> segment, int operationId) =0;
        virtual int ClimbSegment(int operationId, int nInt, vector<Point>& tempNewPoint, const Polygon& polygon_to_cut, Vector2d vectorSegment, Polygon& tempPolygon) = 0;
        virtual int DescentSegment(int operationId, int nInt, vector<Point>& tempNewPoint, const Polygon& polygon_to_cut, Vector2d vectorSegment, Polygon& tempPolygon) = 0;
};

class WorkPolygon : public IWorkPolygon {
    private:
        double _toleranceContained;
        unordered_map<int, vector<Polygon>> _operations;
        unordered_map<int, vector<Point>> _newPoints;

    public:
        WorkPolygon();
        int CutPolygon(Polygon polygon_to_cut,
                       vector<Point> segment);
        vector<Polygon>& GetCuttedPolygons(int operationId);
        vector<Point>& GetNewPoints(int operationId);
        double Contained(Point p_2, Point p_0, Point p_1);
        vector<Point> CreateVectorNewPoint(vector<Point> intersectionPoints, vector<double> parametricCoordinates, const Polygon& polygon, vector<Point> segment, int operationId);
        int ClimbSegment(int operationId, int nInt, vector<Point>& tempNewPoint, const Polygon& polygon_to_cut, Vector2d vectorSegment, Polygon& tempPolygon);
        int DescentSegment(int operationId, int nInt, vector<Point>& tempNewPoint, const Polygon& polygon_to_cut, Vector2d vectorSegment, Polygon& tempPolygon);
};
}

#endif // CUTPOLYGONCLASS_H

