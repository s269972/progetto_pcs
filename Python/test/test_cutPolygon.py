from unittest import TestCase
import numpy as np

import src.DTO as DTO
import src.CreatePolygon as CreatePolygon
import src.CutPolygon as CutPolygon
import src.ComputeIntersection as ComputeIntersection

class TestGeometryFactory(TestCase):
    @staticmethod
    def FillPolygon1Vertices() -> []:
        vertices1 = [DTO.Point(1.0000e+00, 1.0000e+00),
                     DTO.Point(5.0000e-00, 1.0000e+00),
                     DTO.Point(5.0000e-00, 3.1000e+00),
                     DTO.Point(1.0000e+00, 3.1000e+00)]

        return vertices1

    def test_create_polygon1(self):
        vertices1 = TestGeometryFactory.FillPolygon1Vertices()

        geometryFactory = CreatePolygon.GeometryFactory()

        try:
            self.assertEqual(geometryFactory.CreatePolygon(vertices1), 1)
        except Exception as ex:
            self.fail()

    def test_get_polygon1(self):
        vertices1 = TestGeometryFactory.FillPolygon1Vertices()

        geometryFactory = CreatePolygon.GeometryFactory()

        try:
            idPolygon = geometryFactory.CreatePolygon(vertices1)
            polygon1 = geometryFactory.GetPolygon(idPolygon)

            self.assertEqual(polygon1.polygonPoints[1].X, vertices1[1].X)
            self.assertEqual(polygon1.polygonPoints[1].Y, vertices1[1].Y)
            self.assertEqual(polygon1.vertices[0], 0)

        except Exception as ex:
            self.fail()

    def test_get_polygon1_num_vertices(self):
        vertices1 = TestGeometryFactory.FillPolygon1Vertices()

        geometryFactory = CreatePolygon.GeometryFactory()

        try:
            geometryFactory.CreatePolygon(vertices1)
            self.assertEqual(geometryFactory.GetPolygonNumberVertices(1), 4)

            geometryFactory.GetPolygon(1)
        except Exception as ex:
            self.fail()

    def test_get_polygon1_vertex(self):
        vertices1 = TestGeometryFactory.FillPolygon1Vertices()

        geometryFactory = CreatePolygon.GeometryFactory()

        try:
            geometryFactory.CreatePolygon(vertices1)

            vertex = geometryFactory.GetPolygonVertex(1, 2)
            self.assertEqual(vertex.X, vertices1[2].X)
            self.assertEqual(vertex.Y, vertices1[2].Y)
        except Exception as ex:
            self.fail()

    def test_get_polygon1_failed(self):
        vertices1 = TestGeometryFactory.FillPolygon1Vertices()

        geometryFactory = CreatePolygon.GeometryFactory()
        geometryFactory.CreatePolygon(vertices1)

        try:
            geometryFactory.GetPolygon(12)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Polygon not found")

    def test_get_polygon1_vertex_failed(self):
        vertices1 = TestGeometryFactory.FillPolygon1Vertices()

        geometryFactory = CreatePolygon.GeometryFactory()
        geometryFactory.CreatePolygon(vertices1)

        try:
            geometryFactory.GetPolygonVertex(12, 1)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Polygon not found")

        try:
            geometryFactory.GetPolygonVertex(1, 17)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Vertex not found")

    @staticmethod
    def FillPolygon2Vertices() -> []:
        vertices2 = [DTO.Point(2.5000e+00, 1.0000e+00),
                     DTO.Point(4.0000e-00, 2.1000e+00),
                     DTO.Point(3.4000e-00, 4.2000e+00),
                     DTO.Point(1.6000e+00, 4.2000e+00),
                     DTO.Point(1.0000e+00, 2.1000e+00)]

        return vertices2

    def test_create_polygon2(self):
        vertices2 = TestGeometryFactory.FillPolygon2Vertices()

        geometryFactory = CreatePolygon.GeometryFactory()

        try:
            self.assertEqual(geometryFactory.CreatePolygon(vertices2), 1)
        except Exception as ex:
            self.fail()

    def test_get_polygon2(self):
        vertices2 = TestGeometryFactory.FillPolygon2Vertices()

        geometryFactory = CreatePolygon.GeometryFactory()

        try:
            idPolygon = geometryFactory.CreatePolygon(vertices2)
            polygon2 = geometryFactory.GetPolygon(idPolygon)

            self.assertEqual(polygon2.polygonPoints[1].X, vertices2[1].X)
            self.assertEqual(polygon2.polygonPoints[1].Y, vertices2[1].Y)
            self.assertEqual(polygon2.vertices[0], 0)

        except Exception as ex:
            self.fail()

    def test_get_polygon2_num_vertices(self):
        vertices2 = TestGeometryFactory.FillPolygon2Vertices()

        geometryFactory = CreatePolygon.GeometryFactory()

        try:
            geometryFactory.CreatePolygon(vertices2)
            self.assertEqual(geometryFactory.GetPolygonNumberVertices(1), 5)

            geometryFactory.GetPolygon(1)
        except Exception as ex:
            self.fail()

    def test_get_polygon2_vertex(self):
        vertices2 = TestGeometryFactory.FillPolygon2Vertices()

        geometryFactory = CreatePolygon.GeometryFactory()

        try:
            geometryFactory.CreatePolygon(vertices2)

            vertex = geometryFactory.GetPolygonVertex(1, 2)
            self.assertEqual(vertex.X, vertices2[2].X)
            self.assertEqual(vertex.Y, vertices2[2].Y)
        except Exception as ex:
            self.fail()

    def test_get_polygon2_failed(self):
        vertices2 = TestGeometryFactory.FillPolygon2Vertices()

        geometryFactory = CreatePolygon.GeometryFactory()
        geometryFactory.CreatePolygon(vertices2)

        try:
            geometryFactory.GetPolygon(12)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Polygon not found")

    def test_get_polygon2_vertex_failed(self):
        vertices2 = TestGeometryFactory.FillPolygon2Vertices()

        geometryFactory = CreatePolygon.GeometryFactory()
        geometryFactory.CreatePolygon(vertices2)

        try:
            geometryFactory.GetPolygonVertex(12, 1)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Polygon not found")

        try:
            geometryFactory.GetPolygonVertex(1, 17)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Vertex not found")

    @staticmethod
    def FillPolygon3Vertices() -> []:
        vertices3 = [DTO.Point(1.5000e+00, 1.0000e+00),
                     DTO.Point(5.6000e-00, 1.5000e+00),
                     DTO.Point(5.5000e-00, 4.8000e+00),
                     DTO.Point(4.0000e+00, 6.2000e+00),
                     DTO.Point(3.2000e+00, 4.2000e+00),
                     DTO.Point(1.0000e+00, 4.0000e+00)]

        return vertices3

    def test_create_polygon3(self):
        vertices3 = TestGeometryFactory.FillPolygon3Vertices()

        geometryFactory = CreatePolygon.GeometryFactory()

        try:
            self.assertEqual(geometryFactory.CreatePolygon(vertices3), 1)
        except Exception as ex:
            self.fail()

    def test_get_polygon3(self):
        vertices3 = TestGeometryFactory.FillPolygon3Vertices()

        geometryFactory = CreatePolygon.GeometryFactory()

        try:
            idPolygon = geometryFactory.CreatePolygon(vertices3)
            polygon3 = geometryFactory.GetPolygon(idPolygon)

            self.assertEqual(polygon3.polygonPoints[1].X, vertices3[1].X)
            self.assertEqual(polygon3.polygonPoints[1].Y, vertices3[1].Y)
            self.assertEqual(polygon3.vertices[0], 0)

        except Exception as ex:
            self.fail()

    def test_get_polygon3_num_vertices(self):
        vertices3 = TestGeometryFactory.FillPolygon3Vertices()

        geometryFactory = CreatePolygon.GeometryFactory()

        try:
            geometryFactory.CreatePolygon(vertices3)
            self.assertEqual(geometryFactory.GetPolygonNumberVertices(1), 6)

            geometryFactory.GetPolygon(1)
        except Exception as ex:
            self.fail()

    def test_get_polygon3_vertex(self):
        vertices3 = TestGeometryFactory.FillPolygon3Vertices()

        geometryFactory = CreatePolygon.GeometryFactory()

        try:
            geometryFactory.CreatePolygon(vertices3)

            vertex = geometryFactory.GetPolygonVertex(1, 2)
            self.assertEqual(vertex.X, vertices3[2].X)
            self.assertEqual(vertex.Y, vertices3[2].Y)
        except Exception as ex:
            self.fail()

    def test_get_polygon3_failed(self):
        vertices3 = TestGeometryFactory.FillPolygon3Vertices()

        geometryFactory = CreatePolygon.GeometryFactory()
        geometryFactory.CreatePolygon(vertices3)

        try:
            geometryFactory.GetPolygon(12)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Polygon not found")

    def test_get_polygon3_vertex_failed(self):
        vertices3 = TestGeometryFactory.FillPolygon3Vertices()

        geometryFactory = CreatePolygon.GeometryFactory()
        geometryFactory.CreatePolygon(vertices3)

        try:
            geometryFactory.GetPolygonVertex(12, 1)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Polygon not found")

        try:
            geometryFactory.GetPolygonVertex(1, 17)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Vertex not found")

    @staticmethod
    def FillPolygonDAuriaVertices() -> []:
        verticesDA = [DTO.Point(2.0000e+00, -2.0000e+00),
                      DTO.Point(0.0000e-00, -1.0000e+00),
                      DTO.Point(3.0000e-00, 1.0000e+00),
                      DTO.Point(0.0000e+00, 2.0000e+00),
                      DTO.Point(3.0000e+00, 2.0000e+00),
                      DTO.Point(3.0000e+00, 3.0000e+00),
                      DTO.Point(-1.0000e+00, 3.0000e+00),
                      DTO.Point(-3.0000e+00, 1.0000e+00),
                      DTO.Point(0.0000e+00, 0.0000e+00),
                      DTO.Point(-3.0000e+00, -2.0000e+00)]

        return verticesDA

    def test_create_polygonDA(self):
        verticesDA = TestGeometryFactory.FillPolygonDAuriaVertices()

        geometryFactory = CreatePolygon.GeometryFactory()

        try:
            self.assertEqual(geometryFactory.CreatePolygon(verticesDA), 1)
        except Exception as ex:
            self.fail()

    def test_get_polygonDA(self):
        verticesDA = TestGeometryFactory.FillPolygonDAuriaVertices()

        geometryFactory = CreatePolygon.GeometryFactory()

        try:
            idPolygon = geometryFactory.CreatePolygon(verticesDA)
            polygonDA = geometryFactory.GetPolygon(idPolygon)

            self.assertEqual(polygonDA.polygonPoints[1].X, verticesDA[1].X)
            self.assertEqual(polygonDA.polygonPoints[1].Y, verticesDA[1].Y)
            self.assertEqual(polygonDA.vertices[0], 0)

        except Exception as ex:
            self.fail()

    def test_get_polygonDA_num_vertices(self):
        verticesDA = TestGeometryFactory.FillPolygonDAuriaVertices()

        geometryFactory = CreatePolygon.GeometryFactory()

        try:
            geometryFactory.CreatePolygon(verticesDA)
            self.assertEqual(geometryFactory.GetPolygonNumberVertices(1), 10)

            geometryFactory.GetPolygon(1)
        except Exception as ex:
            self.fail()

    def test_get_polygonDA_vertex(self):
        verticesDA = TestGeometryFactory.FillPolygonDAuriaVertices()

        geometryFactory = CreatePolygon.GeometryFactory()

        try:
            geometryFactory.CreatePolygon(verticesDA)

            vertex = geometryFactory.GetPolygonVertex(1, 2)
            self.assertEqual(vertex.X, verticesDA[2].X)
            self.assertEqual(vertex.Y, verticesDA[2].Y)

        except Exception as ex:
            self.fail()

    def test_get_polygonDA_failed(self):
        verticesDA = TestGeometryFactory.FillPolygonDAuriaVertices()

        geometryFactory = CreatePolygon.GeometryFactory()
        geometryFactory.CreatePolygon(verticesDA)

        try:
            geometryFactory.GetPolygon(12)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Polygon not found")

    def test_get_polygonDA_vertex_failed(self):
        verticesDA = TestGeometryFactory.FillPolygonDAuriaVertices()

        geometryFactory = CreatePolygon.GeometryFactory()
        geometryFactory.CreatePolygon(verticesDA)

        try:
            geometryFactory.GetPolygonVertex(12, 1)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Polygon not found")

        try:
            geometryFactory.GetPolygonVertex(1, 17)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Vertex not found")

    @staticmethod
    def FillPolygonConvexVertices() -> []:
        verticesC = [DTO.Point(2.0000e+00, 2.0000e+00),
                     DTO.Point(5.0000e-00, 1.0000e+00),
                     DTO.Point(6.0000e-00, 4.0000e+00),
                     DTO.Point(4.0000e+00, 7.0000e+00),
                     DTO.Point(2.0000e+00, 6.0000e+00),
                     DTO.Point(1.0000e+00, 4.0000e+00)]

        return verticesC

    def test_create_polygonC(self):
        verticesC = TestGeometryFactory.FillPolygonConvexVertices()

        geometryFactory = CreatePolygon.GeometryFactory()

        try:
            self.assertEqual(geometryFactory.CreatePolygon(verticesC), 1)
        except Exception as ex:
            self.fail()

    def test_get_polygonC(self):
        verticesC = TestGeometryFactory.FillPolygonConvexVertices()

        geometryFactory = CreatePolygon.GeometryFactory()

        try:
            idPolygon = geometryFactory.CreatePolygon(verticesC)
            polygonC = geometryFactory.GetPolygon(idPolygon)

            self.assertEqual(polygonC.polygonPoints[1].X, verticesC[1].X)
            self.assertEqual(polygonC.polygonPoints[1].Y, verticesC[1].Y)
            self.assertEqual(polygonC.vertices[0], 0)

        except Exception as ex:
            self.fail()

    def test_get_polygonC_num_vertices(self):
        verticesC = TestGeometryFactory.FillPolygonConvexVertices()

        geometryFactory = CreatePolygon.GeometryFactory()

        try:
            geometryFactory.CreatePolygon(verticesC)
            self.assertEqual(geometryFactory.GetPolygonNumberVertices(1), 6)

            geometryFactory.GetPolygon(1)
        except Exception as ex:
            self.fail()

    def test_get_polygonC_vertex(self):
        verticesC = TestGeometryFactory.FillPolygonConvexVertices()

        geometryFactory = CreatePolygon.GeometryFactory()

        try:
            geometryFactory.CreatePolygon(verticesC)

            vertex = geometryFactory.GetPolygonVertex(1, 2)
            self.assertEqual(vertex.X, verticesC[2].X)
            self.assertEqual(vertex.Y, verticesC[2].Y)
        except Exception as ex:
            self.fail()

    def test_get_polygonC_failed(self):
        verticesC = TestGeometryFactory.FillPolygonConvexVertices()

        geometryFactory = CreatePolygon.GeometryFactory()
        geometryFactory.CreatePolygon(verticesC)

        try:
            geometryFactory.GetPolygon(12)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Polygon not found")

    def test_get_polygonC_vertex_failed(self):
        verticesC = TestGeometryFactory.FillPolygonConvexVertices()

        geometryFactory = CreatePolygon.GeometryFactory()
        geometryFactory.CreatePolygon(verticesC)

        try:
            geometryFactory.GetPolygonVertex(12, 1)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Polygon not found")

        try:
            geometryFactory.GetPolygonVertex(1, 17)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Vertex not found")

class TestIntersector1D1D(TestCase):

    @staticmethod
    def FillSegment1Vertices() -> []:
        segment1 = [DTO.Point(2.0000e+00, 1.2000e+00),
                    DTO.Point(4.0000e+00, 3.0000e+00)]

        return segment1

    def test_compute_intersection1(self):
        vertices1 = TestGeometryFactory.FillPolygon1Vertices()

        geometryFactory = CreatePolygon.GeometryFactory()
        idPolygon = geometryFactory.CreatePolygon(vertices1)
        polygon1 = geometryFactory.GetPolygon(idPolygon)

        segment1 = TestIntersector1D1D.FillSegment1Vertices()
        intersector1D1D = ComputeIntersection.Intersector1D1D()

        try:
            self.assertEqual(intersector1D1D.GetPolygonNumberVertices(polygon1), 4)
            numIntersectionPoints = intersector1D1D.ComputeIntersection(polygon1, segment1)
            self.assertEqual(numIntersectionPoints, 2)
            self.assertEqual(intersector1D1D.IntersectionPoint(0).X, 1.7777777777777777)
            self.assertEqual(intersector1D1D.IntersectionPoint(0).Y, 1.0)
            self.assertEqual(intersector1D1D.IntersectionPoint(1).X, 4.111111111111112)
            self.assertEqual(intersector1D1D.IntersectionPoint(1).Y, 3.1000000000000005)

        except Exception as ex:
            self.fail()

    @staticmethod
    def FillSegment2Vertices() -> []:
        segment2 = [DTO.Point(1.4000e+00, 2.7500e+00),
                    DTO.Point(3.6000e+00, 2.2000e+00)]

        return segment2

    def test_compute_intersection2(self):
        vertices2 = TestGeometryFactory.FillPolygon2Vertices()

        geometryFactory = CreatePolygon.GeometryFactory()
        idPolygon = geometryFactory.CreatePolygon(vertices2)
        polygon2 = geometryFactory.GetPolygon(idPolygon)

        segment2 = TestIntersector1D1D.FillSegment2Vertices()
        intersector1D1D = ComputeIntersection.Intersector1D1D()

        try:
            self.assertEqual(intersector1D1D.GetPolygonNumberVertices(polygon2), 5)
            numIntersectionPoints = intersector1D1D.ComputeIntersection(polygon2, segment2)
            self.assertEqual(numIntersectionPoints, 2)
            self.assertEqual(round(intersector1D1D.IntersectionPoint(0).X, 2), polygon2.polygonPoints[1].X)
            self.assertEqual(round(intersector1D1D.IntersectionPoint(0).Y, 2), polygon2.polygonPoints[1].Y)
            self.assertEqual(intersector1D1D.IntersectionPoint(1).X, 1.2000e+00)
            self.assertEqual(intersector1D1D.IntersectionPoint(1).Y, 2.8000e+00)

        except Exception as ex:
            self.fail()

    @staticmethod
    def FillSegment3Vertices() -> []:
        segment3 = [DTO.Point(2.0000e+00, 3.7000e+00),
                    DTO.Point(4.1000e+00, 5.9000e+00)]

        return segment3

    def test_compute_intersection3(self):
        vertices3 = TestGeometryFactory.FillPolygon3Vertices()

        geometryFactory = CreatePolygon.GeometryFactory()
        idPolygon = geometryFactory.CreatePolygon(vertices3)
        polygon3 = geometryFactory.GetPolygon(idPolygon)

        segment3 = TestIntersector1D1D.FillSegment3Vertices()
        intersector1D1D = ComputeIntersection.Intersector1D1D()

        try:
            self.assertEqual(intersector1D1D.GetPolygonNumberVertices(polygon3), 6)
            numIntersectionPoints = intersector1D1D.ComputeIntersection(polygon3, segment3)
            self.assertEqual(numIntersectionPoints, 4)
            self.assertEqual(intersector1D1D.IntersectionPoint(0).X, 4.2043269230769225)
            self.assertEqual(intersector1D1D.IntersectionPoint(0).Y, 6.0092948717948715)
            self.assertEqual(intersector1D1D.IntersectionPoint(1).X, 3.721311475409836)
            self.assertEqual(intersector1D1D.IntersectionPoint(1).Y, 5.503278688524591)
            self.assertEqual(intersector1D1D.IntersectionPoint(2).X, 2.408597285067873)
            self.assertEqual(intersector1D1D.IntersectionPoint(2).Y, 4.128054298642534)
            self.assertEqual(intersector1D1D.IntersectionPoint(3).X, 1.1912162162162163)
            self.assertEqual(intersector1D1D.IntersectionPoint(3).Y, 2.852702702702703)

        except Exception as ex:
            self.fail()

    @staticmethod
    def FillSegment1DAuriaVertices() -> []:
        segmentDA1 = [DTO.Point(0.0000e+00, -3.0000e+00),
                     DTO.Point(0.0000e+00, 4.0000e+00)]

        return segmentDA1

    @staticmethod
    def FillSegment2DAuriaVertices() -> []:
        segmentDA2 = [DTO.Point(-4.0000e+00, -4.0000e+00),
                     DTO.Point(4.0000e+00, 4.0000e+00)]

        return segmentDA2

    def test_compute_intersectionDAuria(self):
        verticesDA = TestGeometryFactory.FillPolygonDAuriaVertices()

        geometryFactory = CreatePolygon.GeometryFactory()
        idPolygon = geometryFactory.CreatePolygon(verticesDA)
        polygonDA = geometryFactory.GetPolygon(idPolygon)

        segmentDA1 = TestIntersector1D1D.FillSegment1DAuriaVertices()
        segmentDA2 = TestIntersector1D1D.FillSegment2DAuriaVertices()

        try:
            intersector1D1D = ComputeIntersection.Intersector1D1D()
            self.assertEqual(intersector1D1D.GetPolygonNumberVertices(polygonDA),10)
            numIntersectionPoints = intersector1D1D.ComputeIntersection(polygonDA, segmentDA1)
            self.assertEqual(numIntersectionPoints, 5)
            self.assertEqual(round(intersector1D1D.IntersectionPoint(0).X, 2), polygonDA.polygonPoints[1].X)
            self.assertEqual(round(intersector1D1D.IntersectionPoint(0).Y, 2), polygonDA.polygonPoints[1].Y)
            self.assertEqual(round(intersector1D1D.IntersectionPoint(1).X, 2), polygonDA.polygonPoints[3].X)
            self.assertEqual(round(intersector1D1D.IntersectionPoint(1).Y, 2), polygonDA.polygonPoints[3].Y)
            self.assertEqual(round(intersector1D1D.IntersectionPoint(2).X, 2), 0.0000e+00)
            self.assertEqual(round(intersector1D1D.IntersectionPoint(2).Y, 2), 3.0000e+00)
            self.assertEqual(round(intersector1D1D.IntersectionPoint(3).X, 2), polygonDA.polygonPoints[8].X)
            self.assertEqual(round(intersector1D1D.IntersectionPoint(3).Y, 2), polygonDA.polygonPoints[8].Y)
            self.assertEqual(round(intersector1D1D.IntersectionPoint(4).X, 2), 0.0000e+00)
            self.assertEqual(round(intersector1D1D.IntersectionPoint(4).Y, 2), -2.0000e+00)

        except Exception as ex:
            self.fail()

        try:
            intersector1D1D = ComputeIntersection.Intersector1D1D()
            self.assertEqual(intersector1D1D.GetPolygonNumberVertices(polygonDA),10)
            numIntersectionPoints = intersector1D1D.ComputeIntersection(polygonDA, segmentDA2)
            self.assertEqual(numIntersectionPoints, 5)
            self.assertEqual(round(intersector1D1D.IntersectionPoint(0).X, 2), 1.5000e+00)
            self.assertEqual(round(intersector1D1D.IntersectionPoint(0).Y, 2), 1.5000e+00)
            self.assertEqual(round(intersector1D1D.IntersectionPoint(1).X, 2), 2.0000e+00)
            self.assertEqual(round(intersector1D1D.IntersectionPoint(1).Y, 2), 2.0000e+00)
            self.assertEqual(round(intersector1D1D.IntersectionPoint(2).X, 2), polygonDA.polygonPoints[5].X)
            self.assertEqual(round(intersector1D1D.IntersectionPoint(2).Y, 2), polygonDA.polygonPoints[5].X)
            self.assertEqual(round(intersector1D1D.IntersectionPoint(3).X, 2), polygonDA.polygonPoints[8].X)
            self.assertEqual(round(intersector1D1D.IntersectionPoint(3).Y, 2), polygonDA.polygonPoints[8].Y)
            self.assertEqual(round(intersector1D1D.IntersectionPoint(4).X, 2), -2.0000e+00)
            self.assertEqual(round(intersector1D1D.IntersectionPoint(4).Y, 2), -2.0000e+00)

        except Exception as ex:
            self.fail()

    @staticmethod
    def FillConvexSegmentVertices() -> []:
        segmentC = [DTO.Point(3.0000e+00, 0.0000e+00),
                    DTO.Point(5.0000e+00, 4.0000e+00)]

        return segmentC

    def test_compute_intersectionConvex(self):
        verticesC = TestGeometryFactory.FillPolygonConvexVertices()

        geometryFactory = CreatePolygon.GeometryFactory()
        idPolygon = geometryFactory.CreatePolygon(verticesC)
        polygonC = geometryFactory.GetPolygon(idPolygon)

        segmentC = TestIntersector1D1D.FillConvexSegmentVertices()
        intersector1D1D = ComputeIntersection.Intersector1D1D()

        try:
            self.assertEqual(intersector1D1D.GetPolygonNumberVertices(polygonC), 6)
            numIntersectionPoints = intersector1D1D.ComputeIntersection(polygonC, segmentC)
            self.assertEqual(numIntersectionPoints, 2)
            self.assertEqual(intersector1D1D.IntersectionPoint(0).X, 3.714285714285714)
            self.assertEqual(intersector1D1D.IntersectionPoint(0).Y, 1.4285714285714282)
            self.assertEqual(intersector1D1D.IntersectionPoint(1).X, 5.428571428571428)
            self.assertEqual(intersector1D1D.IntersectionPoint(1).Y, 4.857142857142856)

        except Exception as ex:
            self.fail()

class TestCutPolygon(TestCase):

    def test_cut_polygon1(self):
        #create polygon and segment
        vertices1 = TestGeometryFactory.FillPolygon1Vertices()
        geometryFactory = CreatePolygon.GeometryFactory()
        idPolygon = geometryFactory.CreatePolygon(vertices1)
        polygon1 = geometryFactory.GetPolygon(idPolygon)
        segment1 = TestIntersector1D1D.FillSegment1Vertices()

        #compute the intersection between the polygon and the segment, and save the results
        intersector1D1D = ComputeIntersection.Intersector1D1D()
        numIntersectionPoints = intersector1D1D.ComputeIntersection(polygon1, segment1)
        intersectionPoints = intersector1D1D._intersectionPoints
        parametricCoordinates = intersector1D1D._everyResultParametricCoordinates

        #Create parameters to method ClimbSegment and DescentiSegment
        numVertices = 4
        vectorSegment = np.zeros(2)
        vectorSegment[0] = segment1[1].X - segment1[0].X
        vectorSegment[1] = segment1[1].Y - segment1[0].Y

        tempPolygonClimb = DTO.Polygon()
        tempPolygonClimb.vertices.append(0)
        tempPolygonClimb.vertices.append(4)
        tempPolygonClimb.polygonPoints.append(vertices1[0])
        tempPolygonClimb.polygonPoints.append(intersectionPoints[0])

        tempPolygonDescent = DTO.Polygon()
        tempPolygonDescent.vertices.append(1)
        tempPolygonDescent.vertices.append(2)
        tempPolygonDescent.vertices.append(7)
        tempPolygonDescent.polygonPoints.append(vertices1[1])
        tempPolygonDescent.polygonPoints.append(vertices1[2])
        tempPolygonClimb.polygonPoints.append(intersectionPoints[1])
        try:
            workPolygon = CutPolygon.WorkPolygon()
            operationId = workPolygon.CutPolygon(polygon1,
                                                 segment1)
            self.assertEqual(operationId, 1)
            cuttedPolygons = workPolygon.GetCuttedPolygons(operationId)
            self.assertEqual(cuttedPolygons[1].vertices[1], 2)
            self.assertEqual(cuttedPolygons[0].vertices[3], 6)
            newPoints = workPolygon.GetNewPoints(operationId)
            self.assertEqual(newPoints[3].X, vertices1[3].X)
            self.assertEqual(newPoints[3].Y, vertices1[3].Y)
            self.assertEqual(newPoints[5].X, segment1[0].X)
            self.assertEqual(newPoints[5].Y, segment1[0].Y)
            self.assertEqual(newPoints[7].X, intersectionPoints[1].X)
            self.assertEqual(newPoints[7].Y, intersectionPoints[1].Y)
            tempNewPoint = workPolygon.CreateVectorNewPoint(intersectionPoints, parametricCoordinates,
                                                                             polygon1, segment1, operationId)
            self.assertEqual(tempNewPoint[3].X, vertices1[3].X)
            self.assertEqual(tempNewPoint[3].Y, vertices1[3].Y)
            self.assertEqual(tempNewPoint[5].X, segment1[0].X)
            self.assertEqual(tempNewPoint[5].Y, segment1[0].Y)
            startIntersectionClimb = workPolygon.ClimbSegment(numVertices, operationId, 4, tempNewPoint, polygon1,
                                                              vectorSegment, tempPolygonClimb)
            self.assertEqual(startIntersectionClimb, 2)
            startIntersectionDescent = workPolygon.DescentSegment(numVertices, operationId, 7, tempNewPoint, polygon1,
                                                                  vectorSegment, tempPolygonDescent)
            self.assertEqual(startIntersectionDescent, 0)

        except Exception as ex:
            self.fail()

        try:
            workPolygon = CutPolygon.WorkPolygon()
            workPolygon.GetCuttedPolygons(2)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "CuttedPolygon not found")

        try:
            workPolygon = CutPolygon.WorkPolygon()
            workPolygon.GetNewPoints(2)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Polygon not cutted")

    def test_cut_polygon2(self):
        # create polygon and segment
        vertices2 = TestGeometryFactory.FillPolygon2Vertices()
        geometryFactory = CreatePolygon.GeometryFactory()
        idPolygon = geometryFactory.CreatePolygon(vertices2)
        polygon2 = geometryFactory.GetPolygon(idPolygon)
        segment2 = TestIntersector1D1D.FillSegment2Vertices()

        # compute the intersection between the polygon and the segment, and save the results
        intersector1D1D = ComputeIntersection.Intersector1D1D()
        numIntersectionPoints = intersector1D1D.ComputeIntersection(polygon2, segment2)
        intersectionPoints = intersector1D1D._intersectionPoints

        try:
            workPolygon = CutPolygon.WorkPolygon()
            operationId = workPolygon.CutPolygon(polygon2,
                                                 segment2)
            self.assertEqual(operationId, 1)
            cuttedPolygons = workPolygon.GetCuttedPolygons(operationId)
            self.assertEqual(cuttedPolygons[1].vertices[2], 3)
            self.assertEqual(cuttedPolygons[0].vertices[1], 1)
            newPoints = workPolygon.GetNewPoints(operationId)
            self.assertEqual(newPoints[2].X, vertices2[2].X)
            self.assertEqual(newPoints[2].Y, vertices2[2].Y)
            self.assertEqual(newPoints[5].X, intersectionPoints[1].X)
            self.assertEqual(newPoints[5].Y, intersectionPoints[1].Y)
            self.assertEqual(newPoints[7].X, segment2[1].X)
            self.assertEqual(newPoints[7].Y, segment2[1].Y)
            self.assertEqual(vertices2[1].X, round(intersectionPoints[0].X, 2))
            self.assertEqual(vertices2[1].Y, round(intersectionPoints[0].Y, 2))

        except Exception as ex:
            self.fail()

        try:
            workPolygon = CutPolygon.WorkPolygon()
            workPolygon.GetCuttedPolygons(2)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "CuttedPolygon not found")

        try:
            workPolygon = CutPolygon.WorkPolygon()
            workPolygon.GetNewPoints(2)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Polygon not cutted")

    def test_cut_polygon3(self):
        # create polygon and segment
        vertices3 = TestGeometryFactory.FillPolygon3Vertices()
        geometryFactory = CreatePolygon.GeometryFactory()
        idPolygon = geometryFactory.CreatePolygon(vertices3)
        polygon3 = geometryFactory.GetPolygon(idPolygon)
        segment3 = TestIntersector1D1D.FillSegment3Vertices()

        # compute the intersection between the polygon and the segment, and save the results
        intersector1D1D = ComputeIntersection.Intersector1D1D()
        numIntersectionPoints = intersector1D1D.ComputeIntersection(polygon3, segment3)
        intersectionPoints = intersector1D1D._intersectionPoints

        try:
            workPolygon = CutPolygon.WorkPolygon()
            operationId = workPolygon.CutPolygon(polygon3,
                                                 segment3)
            self.assertEqual(operationId, 1)
            cuttedPolygons = workPolygon.GetCuttedPolygons(operationId)
            self.assertEqual(cuttedPolygons[1].vertices[2], 10)
            self.assertEqual(cuttedPolygons[0].vertices[8], 7)
            self.assertEqual(cuttedPolygons[2].vertices[3], 8)
            newPoints = workPolygon.GetNewPoints(operationId)
            self.assertEqual(newPoints[2].X, vertices3[2].X)
            self.assertEqual(newPoints[2].Y, vertices3[2].Y)
            self.assertEqual(newPoints[6].X, intersectionPoints[3].X)
            self.assertEqual(newPoints[6].Y, intersectionPoints[3].Y)
            self.assertEqual(newPoints[8].X, intersectionPoints[2].X)
            self.assertEqual(newPoints[8].Y, intersectionPoints[2].Y)
            self.assertEqual(newPoints[10].X, segment3[1].X)
            self.assertEqual(newPoints[10].Y, segment3[1].Y)

        except Exception as ex:
            self.fail()

        try:
            workPolygon = CutPolygon.WorkPolygon()
            workPolygon.GetCuttedPolygons(2)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "CuttedPolygon not found")

        try:
            workPolygon = CutPolygon.WorkPolygon()
            workPolygon.GetNewPoints(2)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Polygon not cutted")

    def test_cut_polygonDAuria1(self):
        # create polygon and segments
        verticesDA = TestGeometryFactory.FillPolygonDAuriaVertices()
        geometryFactory = CreatePolygon.GeometryFactory()
        idPolygon = geometryFactory.CreatePolygon(verticesDA)
        polygonDA = geometryFactory.GetPolygon(idPolygon)
        segmentDA1 = TestIntersector1D1D.FillSegment1DAuriaVertices()

        try:
            # compute the intersection between the polygon and the segment, and save the results
            intersector1D1D = ComputeIntersection.Intersector1D1D()
            numIntersectionPoints_1 = intersector1D1D.ComputeIntersection(polygonDA, segmentDA1)
            intersectionPoints_1 = intersector1D1D._intersectionPoints
            workPolygon = CutPolygon.WorkPolygon()
            operationId = workPolygon.CutPolygon(polygonDA,
                                                 segmentDA1)
            self.assertEqual(operationId, 1)
            cuttedPolygons = workPolygon.GetCuttedPolygons(operationId)
            self.assertEqual(cuttedPolygons[1].vertices[1], 2)
            self.assertEqual(cuttedPolygons[0].vertices[2], 10)
            self.assertEqual(cuttedPolygons[2].vertices[0], 3)
            self.assertEqual(cuttedPolygons[3].vertices[4], 11)
            self.assertEqual(cuttedPolygons[4].vertices[3], 1)
            newPoints = workPolygon.GetNewPoints(operationId)
            self.assertEqual(newPoints[2].X, verticesDA[2].X)
            self.assertEqual(newPoints[2].Y, verticesDA[2].Y)
            self.assertEqual(newPoints[6].X, verticesDA[6].X)
            self.assertEqual(newPoints[6].Y, verticesDA[6].Y)
            self.assertEqual(newPoints[8].X, intersectionPoints_1[3].X)
            self.assertEqual(newPoints[8].Y, intersectionPoints_1[3].Y)
            self.assertEqual(newPoints[10].X, intersectionPoints_1[4].X)
            self.assertEqual(newPoints[10].Y, intersectionPoints_1[4].Y)

        except Exception as ex:
            self.fail()

        try:
            workPolygon = CutPolygon.WorkPolygon()
            workPolygon.GetCuttedPolygons(2)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "CuttedPolygon not found")

        try:
            workPolygon = CutPolygon.WorkPolygon()
            workPolygon.GetNewPoints(2)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Polygon not cutted")

    def test_cut_polygonDAuria2(self):
        # create polygon and segments
        verticesDA = TestGeometryFactory.FillPolygonDAuriaVertices()
        geometryFactory = CreatePolygon.GeometryFactory()
        idPolygon = geometryFactory.CreatePolygon(verticesDA)
        polygonDA = geometryFactory.GetPolygon(idPolygon)
        segmentDA2 = TestIntersector1D1D.FillSegment2DAuriaVertices()

        try:
            # compute the intersection between the polygon and the segment, and save the results
            intersector1D1D = ComputeIntersection.Intersector1D1D()
            numIntersectionPoints_2 = intersector1D1D.ComputeIntersection(polygonDA, segmentDA2)
            intersectionPoints_2 = intersector1D1D._intersectionPoints
            workPolygon = CutPolygon.WorkPolygon()
            operationId = workPolygon.CutPolygon(polygonDA,
                                                 segmentDA2)
            self.assertEqual(operationId, 1)
            cuttedPolygons = workPolygon.GetCuttedPolygons(operationId)
            self.assertEqual(cuttedPolygons[0].vertices[2], 2)
            self.assertEqual(cuttedPolygons[1].vertices[5], 8)
            self.assertEqual(cuttedPolygons[2].vertices[1], 5)
            self.assertEqual(cuttedPolygons[3].vertices[2], 10)
            newPoints = workPolygon.GetNewPoints(operationId)
            self.assertEqual(newPoints[1].X, verticesDA[1].X)
            self.assertEqual(newPoints[1].Y, verticesDA[1].Y)
            self.assertEqual(newPoints[7].X, verticesDA[7].X)
            self.assertEqual(newPoints[7].Y, verticesDA[7].Y)
            self.assertEqual(newPoints[5].X, round(intersectionPoints_2[2].X, 4))
            self.assertEqual(newPoints[5].Y, round(intersectionPoints_2[2].Y, 4))
            self.assertEqual(round(newPoints[10].X, 4), round(intersectionPoints_2[4].X, 4))
            self.assertEqual(round(newPoints[10].Y, 4), round(intersectionPoints_2[4].Y, 4))

        except Exception as ex:
            self.fail()

        try:
            workPolygon = CutPolygon.WorkPolygon()
            workPolygon.GetCuttedPolygons(2)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "CuttedPolygon not found")

        try:
            workPolygon = CutPolygon.WorkPolygon()
            workPolygon.GetNewPoints(2)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Polygon not cutted")

    def test_cut_polygonConvex(self):
        # create polygon and segment
        verticesC = TestGeometryFactory.FillPolygonConvexVertices()
        geometryFactory = CreatePolygon.GeometryFactory()
        idPolygon = geometryFactory.CreatePolygon(verticesC)
        polygonC = geometryFactory.GetPolygon(idPolygon)
        segmentC = TestIntersector1D1D.FillConvexSegmentVertices()

        # compute the intersection between the polygon and the segment, and save the results
        intersector1D1D = ComputeIntersection.Intersector1D1D()
        numIntersectionPoints = intersector1D1D.ComputeIntersection(polygonC, segmentC)
        intersectionPoints = intersector1D1D._intersectionPoints

        try:
            workPolygon = CutPolygon.WorkPolygon()
            operationId = workPolygon.CutPolygon(polygonC,
                                                 segmentC)
            self.assertEqual(operationId, 1)
            cuttedPolygons = workPolygon.GetCuttedPolygons(operationId)
            self.assertEqual(cuttedPolygons[1].vertices[3], 7)
            self.assertEqual(cuttedPolygons[0].vertices[4], 3)
            newPoints = workPolygon.GetNewPoints(operationId)
            self.assertEqual(newPoints[3].X, verticesC[3].X)
            self.assertEqual(newPoints[3].Y, verticesC[3].Y)
            self.assertEqual(newPoints[6].X, intersectionPoints[0].X)
            self.assertEqual(newPoints[6].Y, intersectionPoints[0].Y)
            self.assertEqual(newPoints[7].X, segmentC[1].X)
            self.assertEqual(newPoints[7].Y, segmentC[1].Y)

        except Exception as ex:
            self.fail()

        try:
            workPolygon = CutPolygon.WorkPolygon()
            workPolygon.GetCuttedPolygons(2)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "CuttedPolygon not found")

        try:
            workPolygon = CutPolygon.WorkPolygon()
            workPolygon.GetNewPoints(2)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Polygon not cutted")
