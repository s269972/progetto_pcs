from src import DTO

class IGeometryFactory:
    def CreatePolygon(self,
                      vertices: ()) -> int:
        pass

    def GetPolygon(self,
                   polygonId: int) -> DTO.Polygon:
        pass

    def GetPolygonNumberVertices(self,
                                 polygonId: int) -> int:
        pass

    def GetPolygonVertex(self,
                         polygonId: int,
                         vertexPosition: int) -> DTO.Point:
        pass


class GeometryFactory(IGeometryFactory):
    def __init__(self):
        self._polygons = {}

    def CreatePolygon(self,
                      vertices: []) -> int:

        # Id del poligono creato
        polygon_id = len(self._polygons) + 1
        self._polygons[polygon_id] = DTO.Polygon()

        num_vertices = len(vertices)

        # Riempe l'attributo "_polygons" con i punti passati in input
        # e con i vertici, che partono da 0
        for v in range(0, num_vertices):
            self._polygons[polygon_id].polygonPoints.append(vertices[v])
            self._polygons[polygon_id].vertices.append(v)

        return polygon_id

    # Cerca il poligono da restituire all'interno di "_polygons"
    # usando come chiave di ricerca l'Id passato in input
    def GetPolygon(self,
                   polygonId: int) -> DTO.Polygon:
        if polygonId not in self._polygons:
            raise Exception("Polygon not found")

        return self._polygons[polygonId]

    # Cerca il poligono all'interno di "_polygons" dopo avergli passato l'Id corrispondente,
    # dopodichè viene restituita la lunghezza del vettore di Vertici del poligono trovato
    def GetPolygonNumberVertices(self,
                                 polygonId: int) -> int:
        if polygonId not in self._polygons:
            raise Exception("Polygon not found")

        return len(self._polygons[polygonId].vertices)

    # Cerca il poligono all'interno di "_polygons" dopo avergli passato l'Id corrispondente,
    # dopodichè vengono restituite le coordinate del punto alla posizione passata in input del poligono trovato
    def GetPolygonVertex(self,
                         polygonId: int,
                         vertexPosition: int) -> DTO.Point:
        if polygonId not in self._polygons:
            raise Exception("Polygon not found")

        if vertexPosition >= len(self._polygons[polygonId].vertices):
            raise Exception("Vertex not found")

        return self._polygons[polygonId].polygonPoints[vertexPosition]