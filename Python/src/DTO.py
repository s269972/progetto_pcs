from enum import Enum


class Point:
    def __init__(self, x: float, y: float):
        self.X = x
        self.Y = y


class Polygon:
    def __init__(self):
        self.polygonPoints = []
        self.vertices = []


class Type(Enum):
    NoIntersection = 0
    IntersectionOnLine = 1
    IntersectionOnSegment = 2
    IntersectionParallelOnLine = 3
    IntersectionParallelOnSegment = 4


class Position(Enum):
    Begin = 0
    Inner = 1
    End = 2
    Outer = 3
