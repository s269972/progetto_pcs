import src.DTO as DTO
import numpy as np
from src import ComputeIntersection

class IWorkPolygon:
    def CutPolygon(self, polygon_to_cut: DTO.Polygon,
                   segment: []) -> int:
        pass

    def GetCuttedPolygons(self,
                          operationId: int) -> []:
        pass

    def GetNewPoints(self, operationId: int) -> []:
        pass

    def Contained(self, P_2: DTO.Point, P_0: DTO.Point, P_1: DTO.Point) -> float:
        pass

    def CreateVectorNewPoint(self, intersections: [], parametricCoordinates: [], polygon: DTO.Polygon, segment: [],
                             operationId: int) -> []:
        pass

    def ClimbSegment(self, numVertices: int, operationId: int, nInt: int, tempNewPoint: [], polygon_to_cut: DTO.Polygon,
                     vectorSegment: [], tempPolygon: DTO.Polygon) -> int:
        pass

    def DescentSegment(self, numVertices: int, operationId: int, nInt: int, tempNewPoint: [],
                       polygon_to_cut: DTO.Polygon, vectorSegment: [], tempPolygon: DTO.Polygon) -> int:
        pass


class WorkPolygon(IWorkPolygon):
    def __init__(self):
        self._toleranceContained: float = 1.0E-7
        self._toleranceIntersection: float = 1.0E-7
        self._tolerancePoint: float = 1.0E-7
        self._operations = {}
        self._newPoints = {}

    def CutPolygon(self, polygon_to_cut: DTO.Polygon,
                   segment: []) -> int:

        e = 0
        u = 0
        tempPolygon = DTO.Polygon()
        tempOperation = []
        vectorSegment = np.zeros(2)
        normalVectorEdge = np.zeros(2)

        operationId = len(self._operations) + 1
        self._operations[operationId] = []
        numVertices = len(polygon_to_cut.polygonPoints)

        # Calcolo delle intersezioni e coordinate parametriche rispetto agli estremi del segmento
        intersector1D1D = ComputeIntersection.Intersector1D1D()
        numIntersectionPoints = intersector1D1D.ComputeIntersection(polygon_to_cut, segment)
        positionIntersectionSecondEdge = intersector1D1D._positionIntersectionSecondEdge
        intersections = intersector1D1D._intersectionPoints
        parametricCoordinates = intersector1D1D._everyResultParametricCoordinates

        # Controlla che i punti estremi del segmento siano interni al poligono
        # e si salva l'informazione in un vettore di booleani
        tempNewPoint = self.CreateVectorNewPoint(intersections, parametricCoordinates, polygon_to_cut, segment,
                                                 operationId)

        # Case noIntersection
        if len(intersections) <= 1:
            tempOperation.append(polygon_to_cut)
        else:
            # Calcolo del vettore direzione del segmento
            vectorSegment[0] = tempNewPoint[numVertices + 1].X - tempNewPoint[numVertices].X
            vectorSegment[1] = tempNewPoint[numVertices + 1].Y - tempNewPoint[numVertices].Y

            # In questo vettore salviamo i vertici che ancora non sono stati aggiunti ad alcun poligono
            verticinonvisitati = polygon_to_cut.vertices

            while len(verticinonvisitati) != 0:
                first = True
                # k vertice da cui parte il tempPolygon
                k = verticinonvisitati[0]
                i = k
                while i != numVertices:  # i cicla sui vertici del poligono
                    # Nel caso in cui si ritorni al vertice di partenza del poligono, il ciclo for si chiude e viene pushato il tempPolygon
                    if i % numVertices == k and first == False:
                        break

                    tempPolygon.vertices.append(i)
                    tempPolygon.polygonPoints.append(self._newPoints[operationId][i])
                    first = False
                    # Ciclo che elimina il vertice appena aggiunto al poligono, dal vettore di vertici non visitati
                    for o in range(0, len(verticinonvisitati)):
                        if i == verticinonvisitati[o]:
                            verticinonvisitati.pop(o)
                            break

                    for j in range(0, len(intersections)):
                        # j cicla sulle intersezioni e vede se appartengono al lato in analisi
                        checkEnter = self.Contained(intersections[j], polygon_to_cut.polygonPoints[i],
                                                    polygon_to_cut.polygonPoints[(i + 1) % numVertices])
                        if checkEnter < self._toleranceContained and checkEnter > -self._toleranceContained:
                            # Controllo se l'intersezione coincide con un vertice e in quel caso aggiungo tale vertice al tempPolygon, ignorando l'intersezione
                            if positionIntersectionSecondEdge[j] == DTO.Position.End or positionIntersectionSecondEdge[
                                j] == DTO.Position.Begin:
                                # Questo if serve a non aggiungere due volte il vertice, nel caso in cui si stia controllando l'intersezione che coincide con il vertice di partenza del tempPolygon
                                if (intersections[j].X > polygon_to_cut.polygonPoints[i].X + self._tolerancePoint or intersections[j].X < polygon_to_cut.polygonPoints[i].X - self._tolerancePoint or
                                        intersections[j].Y > polygon_to_cut.polygonPoints[i].Y + self._tolerancePoint or intersections[j].Y < polygon_to_cut.polygonPoints[i].Y - self._tolerancePoint):
                                    #Cerca a quale vertice l'intersezione corrisponde
                                    for v in range(0,
                                                   numVertices):  # forse posso togliere un for mettendo l'if della riga prima all'inizio
                                        if (intersections[j].X < polygon_to_cut.polygonPoints[v].X + self._tolerancePoint and intersections[j].X > polygon_to_cut.polygonPoints[v].X - self._tolerancePoint) and (intersections[j].Y < polygon_to_cut.polygonPoints[v].Y + self._tolerancePoint and intersections[j].Y > polygon_to_cut.polygonPoints[v].Y - self._tolerancePoint):
                                            tempPolygon.vertices.append(v)
                                            tempPolygon.polygonPoints.append(self._newPoints[operationId][v])
                                            #Cerca a quale indice di tempNewPoint corrisponde l'intersezione
                                            #il valore di e serve per iniziare a scorrere la retta tagliante
                                            for e in range(numVertices, len(tempNewPoint)):
                                                if (tempNewPoint[e].X < intersections[j].X + self._tolerancePoint and tempNewPoint[e].X > intersections[j].X - self._tolerancePoint) and (tempNewPoint[e].Y < intersections[j].Y + self._tolerancePoint and tempNewPoint[e].Y > intersections[j].Y - self._tolerancePoint):
                                                    break
                                            break

                                # Il ciclo su j viene interrotto quando il tempPolygon parte da un vertice coincidente con un'intersezione
                                # evitando così di scendere sulla retta passante per gli estremi del segmento (vedi poligono 2)
                                else:
                                    break

                            else:
                                # e: questo ciclo cerca a quale indice di vertice corrisponde l'intersezione nel tempNewPoint
                                #il valore di e serve per iniziare a scorrere la retta tagliante
                                for e in range(numVertices, len(tempNewPoint)):
                                    if (tempNewPoint[e].X < intersections[j].X + self._tolerancePoint and tempNewPoint[e].X > intersections[j].X - self._tolerancePoint) and (tempNewPoint[e].Y < intersections[j].Y + self._tolerancePoint and tempNewPoint[e].Y > intersections[j].Y - self._tolerancePoint):
                                        #Cerca a quale elemento di newPoint l'intersezione corrisponde
                                        for u in range(0, len(self._newPoints[operationId])):
                                            if (self._newPoints[operationId][u].X < tempNewPoint[e].X + self._tolerancePoint and self._newPoints[operationId][u].X > tempNewPoint[e].X - self._tolerancePoint) and (tempNewPoint[e].Y < self._newPoints[operationId][u].Y + self._tolerancePoint and tempNewPoint[e].Y > self._newPoints[operationId][u].Y - self._tolerancePoint) :
                                                break
                                        tempPolygon.vertices.append(u)
                                        tempPolygon.polygonPoints.append(self._newPoints[operationId][u])
                                        break

                            # Quando viene aggiunta un'intersezione al poligono, occorre percorrere la retta passante per il segmento fino all'intersezione successiva,
                            # aggiungendo gli eventuali punti interni del segmento

                            # Per trovare la normale al lato si fa una rotazione di 90° in senso orario del vettore direzione del lato (direzione uscente dal poligono)
                            normalVectorEdge[0] = tempNewPoint[i + 1].Y - tempNewPoint[i].Y
                            normalVectorEdge[1] = -(tempNewPoint[i + 1].X - tempNewPoint[i].X)

                            prodScal = normalVectorEdge.dot(vectorSegment)
                            # Se il prodotto scalare tra normale al lato e segmento è minore di 0 significa che si deve percorrere il segmento verso sinistra
                            if prodScal < 0:
                                # Il metodo ClimbSegment aggiunge al tempPolygon tutti i vertici della retta passante per il segmento
                                # fino all'intersezione successiva, percorrendola nel verso delle coordinate parametriche crescenti
                                i = self.ClimbSegment(numVertices, operationId, e, tempNewPoint, polygon_to_cut,
                                                      vectorSegment, tempPolygon)

                            # Se il prodotto scalare tra normale al lato e segmento è maggiore di 0 significa che si deve percorrere il segmento verso destra
                            else:
                                # Il metodo DescentSegment aggiunge al tempPolygon tutti i vertici della retta passante per il segmento
                                # fino all'intersezione successiva, percorrendola nel verso delle coordinate parametriche decrescenti
                                i = self.DescentSegment(numVertices, operationId, e, tempNewPoint, polygon_to_cut,
                                                        vectorSegment, tempPolygon)

                            break

                    i = i + 1

                # Push poligono quando ritorno alla posizione di partenza del for (vhe poi e lo stesso del while)
                self._operations[operationId].append(tempPolygon)
                tempPolygon = DTO.Polygon()

        return operationId

    def GetCuttedPolygons(self, operationId: int) -> []:
        if operationId not in self._operations:
            raise Exception("CuttedPolygon not found")

        return self._operations[operationId]

    def GetNewPoints(self, operationId: int) -> []:
        if operationId not in self._newPoints:
            raise Exception("Polygon not cutted")

        return self._newPoints[operationId]

    def Contained(self, P_2: DTO.Point, P_0: DTO.Point, P_1: DTO.Point) -> float:
        #Calcola il prodotto vettoriale tra vettore che congiunge i due estremi del lato
        #e vettore che congiunge primo estremo e intersezione.
        V = DTO.Point(P_1.X - P_0.X, P_1.Y - P_0.Y)
        W = DTO.Point(P_2.X - P_0.X, P_2.Y - P_0.Y)
        Z = (V.X * W.Y) - (V.Y * W.X)
        return Z

    def CreateVectorNewPoint(self, intersections: [], parametricCoordinates: [], polygon: DTO.Polygon, segment: [],
                             operationId: int) -> []:

        found = False
        NOTCONTAINEDDOWN = np.zeros(2)
        NOTCONTAINEDUP = np.zeros(2)
        NOTCONTAINED = np.zeros(2)
        extraPoints = []
        extraParametricCoordinates = []
        numVertices = len(polygon.polygonPoints)
        SegmentParametricCoordinates = []
        SegmentParametricCoordinates.append(0)
        SegmentParametricCoordinates.append(1)

        # Controlla che i punti estremi del segmento siano interni al poligono
        # e si salva l'informazione in un vettore di interi,
        # lo 0 corrisponde a "è contenuto" mentre l' 1 a "NON è contenuto"
        NOTCONTAINED[0] = 0
        NOTCONTAINED[1] = 0
        NOTCONTAINEDDOWN[0] = 0
        NOTCONTAINEDDOWN[1] = 0
        NOTCONTAINEDUP[0] = 0
        NOTCONTAINEDUP[1] = 0

        # Controlla per ognuno dei due punti del segmento
        # se la loro coordinata parametrica, rispetto alla retta a cui il segmento appartiene,
        # è minore o maggiore delle coordinate parametriche sempre relative alla stessa retta
        for j in range(0, 2):
            notContainedDown = 0
            notContainedUp = 0
            for u in range(0, len(intersections)):
                if j < parametricCoordinates[u]:
                    notContainedDown = notContainedDown + 1
            if notContainedDown == len(intersections):
                NOTCONTAINEDDOWN[j] = 1
            for u in range(0, len(intersections)):
                if j > parametricCoordinates[u]:
                    notContainedUp = notContainedUp + 1
            if notContainedUp == len(intersections):
                NOTCONTAINEDUP[j] = 1

        # Se uno dei due casi, per ciascuno dei due punti del segmento, è verificato,
        # significa che il punto non è contenuto nel segmento
        # (o perchè è "al di sotto" (DOWN) del segmento, o perchè è "al di sopra" (UP))
        for j in range(0, 2):
            if NOTCONTAINEDDOWN[j] == 1 or NOTCONTAINEDUP[j] == 1:
                NOTCONTAINED[j] = 1

        # Riempiamo i vettori extraPoint e extraParametricCoordinates con i punti
        # e le rispettive coordinate parametriche degli estremi del segmento interni al poligono
        # e dei punti di intersezione
        for a in range(0, len(intersections) + len(segment)):
            if a < len(segment):
                if (NOTCONTAINED[a] == 0):
                    extraPoints.append(segment[a])
                    extraParametricCoordinates.append(SegmentParametricCoordinates[a])
            else:
                extraPoints.append(intersections[a - len(segment)])
                extraParametricCoordinates.append(parametricCoordinates[a - len(segment)])

        # Ordino le coordinate parametriche in ordine crescente sfruttando l'algoritmo bubblesort
        for i in range(0, len(extraPoints) - 1):
            for j in range(0, len(extraPoints) - i - 1):
                if extraParametricCoordinates[j] > extraParametricCoordinates[j + 1]:
                    vTempPC = extraParametricCoordinates[j]
                    extraParametricCoordinates[j] = extraParametricCoordinates[j + 1]
                    extraParametricCoordinates[j + 1] = vTempPC
                    vTempPoint = extraPoints[j]
                    extraPoints[j] = extraPoints[j + 1]
                    extraPoints[j + 1] = vTempPoint

        # Creiamo il vettore _newPoint che contiene i vertici del poligono, le intersezioni e gli eventuali estremi del segmento interni
        # La posizione che il punto ha in questo vettore corrisponde all'indice del vertice (attenzione perchè tempNewPoint contiene i doppi)
        tempNewPoint = polygon.polygonPoints
        for t in range(0, len(extraPoints)):
            tempNewPoint.append(extraPoints[t])

        # Vettore definitivo di newPoints depurato dai doppi
        self._newPoints[operationId] = []
        for y in range(0, len(tempNewPoint)):
            self._newPoints[operationId].append(tempNewPoint[y])
            for w in range(0, len(self._newPoints[operationId]) - 1):
                if (tempNewPoint[y].X < self._newPoints[operationId][w].X + self._tolerancePoint and tempNewPoint[
                    y].X > self._newPoints[operationId][w].X - self._tolerancePoint) and (
                        tempNewPoint[y].Y < self._newPoints[operationId][w].Y + self._tolerancePoint and
                        tempNewPoint[y].Y > self._newPoints[operationId][w].Y - self._tolerancePoint):
                    self._newPoints[operationId].pop(len(self._newPoints[operationId]) - 1)
                    break
        return tempNewPoint

    def ClimbSegment(self, numVertices: int, operationId: int, nInt: int, tempNewPoint: [], polygon_to_cut: DTO.Polygon,
                     vectorSegment: [], tempPolygon: DTO.Polygon) -> int:
        vectorEdge = np.zeros(2)
        u = 0
        z = 0
        contenuto = False

        # t cicla sui punti da aggiungere controllando se appartengono al lato, cercando quindi la successiva intersezione
        for t in range(nInt + 1, len(tempNewPoint)):
            #Trova a quale indice nel newPoint corrisponde il punto considerato del tempNewPoint
            for u in range(0, len(self._newPoints[operationId])):
                if (tempNewPoint[t].X < self._newPoints[operationId][u].X + self._tolerancePoint and tempNewPoint[t].X > self._newPoints[operationId][u].X - self._tolerancePoint) and (tempNewPoint[t].Y < self._newPoints[operationId][u].Y + self._tolerancePoint and tempNewPoint[t].Y > self._newPoints[operationId][u].Y - self._tolerancePoint):
                    break
            tempPolygon.vertices.append(u)
            tempPolygon.polygonPoints.append(self._newPoints[operationId][u])

            # z cicla sui lati
            # Grazie al flag "contenuto" si interrompe il ciclo sulle intersezioni, quando una di queste appartiene ad un lato alla sinistra del verso di percorrenza del segmento
            for z in range(0, numVertices):
                checkEnter = self.Contained(tempNewPoint[t], polygon_to_cut.polygonPoints[z],
                                            polygon_to_cut.polygonPoints[(z + 1) % numVertices])
                if (checkEnter < self._toleranceContained and checkEnter > -self._toleranceContained):
                    if u < numVertices:  # se l'intersezione coincide con un vertice
                        vectorEdge[0] = self._newPoints[operationId][(u + 1) % numVertices].X - \
                                        self._newPoints[operationId][u].X
                        vectorEdge[1] = self._newPoints[operationId][(u + 1) % numVertices].Y - \
                                        self._newPoints[operationId][u].Y
                        checkLeft = vectorSegment[0] * vectorEdge[1] - vectorSegment[1] * vectorEdge[0]

                        # Se il lato che parte da tale vertice risulta essere a destra del segmento, il ciclo che scorre il segmento e cerca altri punti di intersezione
                        # (ciclo con indice t) continua e "contenuto" rimane settato a false
                        if checkLeft < 0:
                            break

                        # Nel caso in cui invece il lato successivo sia alla sinistra, l'intersezione viene tolta dai vertici del tempPolygon,
                        # in quanto il vertice verrà aggiunto successivamente (nel ciclo principale dei lati, in indice i)
                        # Contenuto viene settato a true e si esce dal ciclo sui punti del segmento che tagliano il poligono (indice t)
                        else:
                            tempPolygon.vertices.pop(len(tempPolygon.vertices) - 1)
                            tempPolygon.polygonPoints.pop(len(tempPolygon.polygonPoints) - 1)
                    contenuto = True
                    break

            if contenuto == True:
                break

        return z

    def DescentSegment(self, numVertices: int, operationId: int, nInt: int, tempNewPoint: [],
                       polygon_to_cut: DTO.Polygon, vectorSegment: [], tempPolygon: DTO.Polygon) -> int:
        vectorEdge = np.zeros(2)
        u = 0
        z = 0
        contenuto = False

        # t cicla sui punti da aggiungere controllando se appartengono al lato
        for t in range(nInt - 1, numVertices - 1, -1):
            #Trova a quale indice nel newPoint corrisponde il punto considerato del tempNewPoint
            for u in range(0, len(self._newPoints[operationId])):
                if (tempNewPoint[t].X < self._newPoints[operationId][u].X + self._tolerancePoint and tempNewPoint[t].X >
                    self._newPoints[operationId][u].X - self._tolerancePoint) and (
                        tempNewPoint[t].Y < self._newPoints[operationId][u].Y + self._tolerancePoint and tempNewPoint[
                    t].Y > self._newPoints[operationId][u].Y - self._tolerancePoint):
                    break
            tempPolygon.vertices.append(u)
            tempPolygon.polygonPoints.append(self._newPoints[operationId][u])

            # z cicla sui lati
            # Grazie al flag "contenuto" si interrompe il ciclo sulle intersezioni, quando una di queste appartiene ad un lato
            for z in range(0, numVertices):
                checkEnter = self.Contained(tempNewPoint[t], polygon_to_cut.polygonPoints[z],
                                            polygon_to_cut.polygonPoints[(z + 1) % numVertices])
                if (checkEnter < self._toleranceContained and checkEnter > -self._toleranceContained):
                    if u < numVertices:  # se l'intersezione coincide con un vertice
                        vectorEdge[0] = self._newPoints[operationId][(u + 1) % numVertices].X - \
                                        self._newPoints[operationId][u].X
                        vectorEdge[1] = self._newPoints[operationId][(u + 1) % numVertices].Y - \
                                        self._newPoints[operationId][u].Y
                        checkLeft = -(vectorSegment[0]) * vectorEdge[1] - (-vectorSegment[1]) * vectorEdge[0]

                        # Se il lato che parte da tale vertice risulta essere a destra del segmento, il ciclo che scorre il segmento e cerca altri punti di intersezione
                        # (ciclo con indice t) continua e "contenuto" rimane settato a false
                        if checkLeft < 0:
                            break

                        # Nel caso in cui invece il lato successivo sia alla sinistra, l'intersezione viene tolta dai vertici del tempPolygon,
                        # in quanto il vertice verrà aggiunto successivamente (nel ciclo principale dei lati, in indice i)
                        # Contenuto viene settato a true e si esce dal ciclo sui punti del segmento che tagliano il poligono (indice t)
                        else:
                            tempPolygon.vertices.pop(len(tempPolygon.vertices) - 1)
                            tempPolygon.polygonPoints.pop(len(tempPolygon.polygonPoints) - 1)

                    contenuto = True
                    break
            if contenuto == True:
                break

        return z