from src import DTO
import numpy as np

class IIntersector1D1D:
    def ComputeIntersection(self, polygon: DTO.Polygon, segment: ()) -> int:
        pass

    def SetFirstSegment(self, origin: DTO.Point, end: DTO.Point):
        pass

    def SetSecondSegment(self, origin: DTO.Point, end: DTO.Point):
        pass

    def GetPolygonNumberVertices(self, polygon_to_cut: DTO.Polygon) -> int:
        pass

    def IntersectionPoint(self, i: int) -> DTO.Point:
        pass


class Intersector1D1D(IIntersector1D1D):
    def __init__(self):
        self._toleranceParallelism: float = 1.0E-7
        self._toleranceIntersection: float = 1.0E-7
        self._resultParametricCoordinates = np.zeros(2)
        self._originFirstSegment = DTO.Point(0, 0)
        self._rightHandSide = np.zeros(2)
        self._matrixTangentVector = np.zeros((2, 2))
        self._type = []
        self._positionIntersectionFirstEdge = []
        self._positionIntersectionSecondEdge = []
        self._everyResultParametricCoordinates = []
        self._intersectionPoints = []

    def ComputeIntersection(self, polygon: DTO.Polygon, segment: []) -> int:

        intersectionType = DTO.Type.NoIntersection
        numVertices = self.GetPolygonNumberVertices(polygon)

        # Crea il segmento che "intersecherà" e "taglierà" il poligono nello spazio bidimiensionale
        # facendo la differenza delle coordinate dei due suoi punti estremi,
        # le due coordinate del segmento vengono poi inserite nella prima colonna della matrice "_matrixTangentVector"
        # come tipo "Vector2d" della libreria di eigen
        # infine viene salvato il primo punto estremo del segmento nell'attributo "_originFirstSegment"
        self.SetFirstSegment(segment[0], segment[1])

        # Calcola l'intersezione tra il segmento tagliante
        # e ogni lato del poligono passato in input alla funzione
        for v in range(0, numVertices):

            # esattamente come per il segmento intersecante,
            # solo che sta volta il segmento ottenuto è il lato del poligono
            # corrispondente alla posizione corrente del ciclo for,
            # ed esso viene salvato nella seconda colonna della matrice "_matrixTangentVector"
            # viene inoltre salvato nell'attributo "_rightHandSide"il vettore che congiunge
            # il primo estremo del segmento tagliante con il primo estremo del lato corrente
            self.SetSecondSegment(polygon.polygonPoints[v], polygon.polygonPoints[(v + 1) % numVertices])
            intersection = 0
            found = 0
            parallelism = np.linalg.det(self._matrixTangentVector)
            check = self._toleranceParallelism * self._toleranceParallelism * np.linalg.norm(
                self._matrixTangentVector[:, 0]) * np.linalg.norm(self._matrixTangentVector[:, 1])

            # Caso in cui il segmento tagliante e il lato corrente NON sono paralleli
            if (parallelism * parallelism) >= check:
                solverMatrix = np.matrix([[self._matrixTangentVector[1, 1], - self._matrixTangentVector[0, 1]],
                                          [-self._matrixTangentVector[1, 0], self._matrixTangentVector[0, 0]]])
                self._resultParametricCoordinates[0] = (solverMatrix[0, 0] * self._rightHandSide[0] + solverMatrix[
                    0, 1] * self._rightHandSide[1]) / parallelism
                self._resultParametricCoordinates[1] = (solverMatrix[1, 0] * self._rightHandSide[0] + solverMatrix[
                    1, 1] * self._rightHandSide[1]) / parallelism

                # Caso in cui l'intersezione avvenga tra il prolungamento del segmento tagliante
                # ed il lato corrente
                if (self._resultParametricCoordinates[1] > - self._toleranceIntersection) and (
                        self._resultParametricCoordinates[1] - 1.0 < self._toleranceIntersection):
                    intersectionType = DTO.Type.IntersectionOnLine
                    intersection = 1

                    # Intersezione incidente tra segmento tagliante e lato corrente
                    if (self._resultParametricCoordinates[0] > - self._toleranceIntersection) and (
                            self._resultParametricCoordinates[0] - 1.0 < self._toleranceIntersection):
                        intersectionType = DTO.Type.IntersectionOnSegment

            # Caso in cui il segmento tagliante e il lato corrente sono paralleli
            else:
                parallelism2 = abs(self._matrixTangentVector[0, 0] * self._rightHandSide[1] - self._rightHandSide[0] *
                                   self._matrixTangentVector[1, 0])
                squaredNormFirstEdge = np.linalg.norm(self._matrixTangentVector[:, 0])
                check2 = self._toleranceParallelism * self._toleranceParallelism * squaredNormFirstEdge * np.linalg.norm(
                    self._rightHandSide, ord=2)

                # Caso in cui il segmento tagliante e il lato corrente appartengono alla stessa retta
                if parallelism2 * parallelism2 <= check2:
                    tempNorm = 1.0 / squaredNormFirstEdge
                    self._resultParametricCoordinates[0] = self._matrixTangentVector[:, 0].dot(
                        self._rightHandSide) * tempNorm
                    self._resultParametricCoordinates[1] = self._resultParametricCoordinates[
                                                               0] - self._matrixTangentVector[:, 0].dot(
                        self._matrixTangentVector[:, 1]) * tempNorm
                    intersection = 1
                    intersectionType = DTO.Type.IntersectionParallelOnLine

                    # Caso in cui il segmento tagliante e il lato corrente si sovrappongono (anche solo in parte)
                    if (self._resultParametricCoordinates[0] > - self._toleranceIntersection and
                        self._resultParametricCoordinates[0] - 1.0 < self._toleranceIntersection) or (
                            self._resultParametricCoordinates[1] > - self._toleranceIntersection and
                            self._resultParametricCoordinates[1] - 1.0 < self._toleranceIntersection):
                        intersectionType = DTO.Type.IntersectionParallelOnSegment
                    else:
                        if (self._resultParametricCoordinates[0] < self._toleranceIntersection) and (
                                self._resultParametricCoordinates[1] - 1.0 > - self._toleranceIntersection):
                            intersectionType = DTO.Type.IntersectionParallelOnSegment

            # Determina la posizione del punto di intersezione rispetto al segmento tagliante
            if self._resultParametricCoordinates[0] < - self._toleranceIntersection or \
                    self._resultParametricCoordinates[0] > 1.0 + self._toleranceIntersection:
                positionIntersectionFirstEdge = DTO.Position.Outer
            elif (self._resultParametricCoordinates[0] > - self._toleranceIntersection) and (
                    self._resultParametricCoordinates[0] < self._toleranceIntersection):
                self._resultParametricCoordinates[0] = 0.0
                positionIntersectionFirstEdge = DTO.Position.Begin
            elif (self._resultParametricCoordinates[0] > 1.0 - self._toleranceIntersection) and (
                    self._resultParametricCoordinates[0] < 1.0 + self._toleranceIntersection):
                self._resultParametricCoordinates[0] = 1.0
                positionIntersectionFirstEdge = DTO.Position.End
            else:
                positionIntersectionFirstEdge = DTO.Position.Inner

            # Determina la posizione del punto di intersezione rispetto al lato corrente
            if self._resultParametricCoordinates[1] < - self._toleranceIntersection or \
                    self._resultParametricCoordinates[1] > 1.0 + self._toleranceIntersection:
                positionIntersectionSecondEdge = DTO.Position.Outer
            elif (self._resultParametricCoordinates[1] > - self._toleranceIntersection) and (
                    self._resultParametricCoordinates[1] < self._toleranceIntersection):
                self._resultParametricCoordinates[1] = 0.0
                positionIntersectionSecondEdge = DTO.Position.Begin
            elif (self._resultParametricCoordinates[1] > 1.0 - self._toleranceIntersection) and (
                    self._resultParametricCoordinates[1] < 1.0 + self._toleranceIntersection):
                self._resultParametricCoordinates[1] = 1.0
                positionIntersectionSecondEdge = DTO.Position.End
            else:
                positionIntersectionSecondEdge = DTO.Position.Inner

            IntersectionPoint = DTO.Point(0, 0)
            IntersectionPoint.X = float(
                segment[0].X + ((segment[1].X - segment[0].X) * self._resultParametricCoordinates[0]))
            IntersectionPoint.Y = float(
                segment[0].Y + ((segment[1].Y - segment[0].Y) * self._resultParametricCoordinates[0]))

            # Fa in modo che non venga inserita più volte una stessa intersezione
            # questo può succedere quando per esempio si ha un'intersezione che cade su un vertice del poligono
            # e l'algoritmo considera l'intersezione su entrambi i lati con quel vertice in comune
            for it in range(0, len(self._intersectionPoints)):
                if (self._intersectionPoints[it].X + self._toleranceIntersection >= IntersectionPoint.X) and (
                        self._intersectionPoints[it].X - self._toleranceIntersection <= IntersectionPoint.X):
                    if (self._intersectionPoints[it].Y + self._toleranceIntersection >= IntersectionPoint.Y) and (
                            self._intersectionPoints[it].Y - self._toleranceIntersection <= IntersectionPoint.Y):
                        found = 1
                        break

            # Se l'intersezione è stata trovata, si salva tutte le informazioni relative all'intersezione
            # nei vari attributi della classe Intersector1D1D
            if (intersection == 1) and (found == 0):
                self._type.append(intersectionType)
                self._positionIntersectionFirstEdge.append(positionIntersectionFirstEdge)
                self._positionIntersectionSecondEdge.append(positionIntersectionSecondEdge)
                self._everyResultParametricCoordinates.append(self._resultParametricCoordinates[0])
                self._intersectionPoints.append(IntersectionPoint)

        return len(self._intersectionPoints)

    def SetFirstSegment(self, origin: DTO.Point, end: DTO.Point):
        self._originFirstSegment = origin
        self._matrixTangentVector[0, 0] = end.X - origin.X
        self._matrixTangentVector[1, 0] = end.Y - origin.Y

    def SetSecondSegment(self, origin: DTO.Point, end: DTO.Point):
        self._matrixTangentVector[0, 1] = origin.X - end.X
        self._matrixTangentVector[1, 1] = origin.Y - end.Y
        self._rightHandSide[0] = (origin.X - self._originFirstSegment.X)
        self._rightHandSide[1] = (origin.Y - self._originFirstSegment.Y)

    def GetPolygonNumberVertices(self, polygon_to_cut: DTO.Polygon) -> int:
        return len(polygon_to_cut.polygonPoints)

    def IntersectionPoint(self, i: int) -> DTO.Point:
        return self._intersectionPoints[i]